﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.ModelsRequest;
using CleanArchitecture.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CleanArchitecture.Api.Controllers
{
    [Route("/")]
    [ApiController]
    public class WebHookController : ControllerBase
    {
        //private readonly IQuestionOptionRepositories _op;

        //private readonly IQuestionOptionRepositories _optionRepo;
        private readonly IHubContext<ChatHub> _hubContext;
        public WebHookController( IHubContext<ChatHub> hubContext)
        {
           // _op = blogService;
            //_optionRepo = optionRepo;
            _hubContext = hubContext;
        }
        [HttpPost("Webhook")]
        public async Task<IActionResult> PostWeb(WebHookDTO data)
        {
            if (data.Dir == "Inbound")
            {
                await _hubContext.Clients.All.SendAsync("SendLid", data.LID);
            }

            return Ok(data);
        }
    }
}
