﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Repositories;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace CleanArchitecture.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MyController : ControllerBase
    {
        private ICustomerRepositories repository ;
        private readonly IHubContext<ChatHub> _hubContext;

        public MyController(IHubContext<ChatHub> hubContext)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<IActionResult> SendMessageToClients(string user, string message)
        {
            RecurringJob.AddOrUpdate("UpdateStatus", () => SendMessagesJob(user, message), "0 17 19 * * *", TimeZoneInfo.Local);

            return Ok();
        }
        [NonAction]
        public async Task SendMessagesJob(string user, string message)
        {
            await PutAllUsersOffline();
            Task.Run(async () =>
            {
                Console.WriteLine("tao đã chạy");

                await _hubContext.Clients.All.SendAsync("ReceiveMessage", user, message);
            }).Wait(); // Block and wait for the asynchronous operation to complete
        }

        [NonAction]
        public async Task PutAllUsersOffline()
        {
            try
            {
                
                var allUsers = repository.FindAllAsync(); // Assuming you have a method to retrieve all users
                List<Customer> listcumtomer = await allUsers;
                if (listcumtomer == null || !listcumtomer.Any())
                {
                    throw new Exception("List of users is null or empty.");
                }

                foreach (var user in listcumtomer)
                {
                    if (user != null)
                    {
                        user.Status = "Off";
                         repository.Update(user);
                        await repository.UnitOfWork.SaveChangesAsync();
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
