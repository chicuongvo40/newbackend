﻿using CleanArchitecture.Api.Controllers.ResponseTypes;
using CleanArchitecture.Application.Contracts.CreateContract;
using CleanArchitecture.Application.Contracts.DeleteContract;
using CleanArchitecture.Application.Contracts.GetContract;
using CleanArchitecture.Application.Contracts.GetContractById;
using CleanArchitecture.Application.Contracts.GetContractByPhone;
using CleanArchitecture.Application.Contracts.GetContractByUserId;
using CleanArchitecture.Application.Contracts.UpdateContract;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace CleanArchitecture.Api.Controllers
{
    [Authorize]
    public class ContractController : ControllerBase
    {
        private readonly ISender _mediator;

        public ContractController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet("api/contract")]
        [ProducesResponseType(typeof(PagedResults<ContractDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PagedResults<ContractDTO>>> GetContract([FromQuery] GetContractCommand command, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
        [HttpGet("api/contract/{id}")]
        [ProducesResponseType(typeof(ContractDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ContractDTO>> GetContractById(int id, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new GetContractByIdCommand { ContractId = id }, cancellationToken);

            if (result != null)
            {
                return Ok(result);
            }
            return NotFound($"Contract with ID {id} not found");
        }
        [HttpGet("api/contract/customerId")]
        [ProducesResponseType(typeof(DetailContract), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<DetailContract>> GetContractByCustomerId(int customerId, CancellationToken cancellationToken = default)
        {
            var command = new GetContractByUserIdCommand { CustomerId = customerId };
            var result = await _mediator.Send(command, cancellationToken);

            if (result != null)
            {
                return Ok(result);
            }

            return NotFound($"No users found with Role ID {customerId}");
        }
        [HttpGet("api/contract/phone")]
        [ProducesResponseType(typeof(List<ContractDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<ContractDTO>>> GetContractByPhone(string phone, CancellationToken cancellationToken = default)
        {
            var command = new GetContractByPhoneCommand { Phone = phone };
            var result = await _mediator.Send(command, cancellationToken);
             
            if (result != null && result.Count > 0)
            {
                return Ok(result);
            }

            return NotFound($"No users found with {phone}");
        }
        //[HttpPost("contract")]
        //[Produces(MediaTypeNames.Application.Json)]
        //[ProducesResponseType(typeof(JsonResponse<Contract>), StatusCodes.Status201Created)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<JsonResponse<Contract>>> CreateContract(
        //[FromBody] CreateContractCommand command,
        //CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(command, cancellationToken);
        //    return CreatedAtAction(nameof(CreateContract), new JsonResponse<Contract>(result));
        //}
        [HttpPost("contract")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(JsonResponse<Contract>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateContract(
        [FromBody] CreateContractCommand command,
        CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return Ok(new { Status = "Success", Message = "Contract Created Successfully!" });
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //[HttpDelete("api/contract/{id}")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult> DeleteContract([FromRoute] int id, CancellationToken cancellationToken = default)
        //{
        //    await _mediator.Send(new DeleteContractCommand(contractid: id), cancellationToken);
        //    return Ok();
        //}
        [HttpDelete("api/contract/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteContract([FromRoute] int id, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(new DeleteContractCommand(contractid: id), cancellationToken);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //[HttpPut("api/contract")]
        //[ProducesResponseType(StatusCodes.Status204NoContent)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult> UpdateContract(
        //    [FromBody] UpdateContractCommand command,
        //    CancellationToken cancellationToken = default)
        //{
        //    try
        //    {
        //        await _mediator.Send(command, cancellationToken);
        //        return NoContent();
        //    }
        //    catch (NotFoundException ex)
        //    {
        //        return NotFound(ex.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing the request.");
        //    }
        //}
        [HttpPut("api/contract")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateContract(
        [FromBody] UpdateContractCommand command,
        CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
