﻿using CleanArchitecture.Api.Controllers.ResponseTypes;
using CleanArchitecture.Application.LogsModel.CreateLogs;
using CleanArchitecture.Application.LogsModel.DeleteLogs;
using CleanArchitecture.Application.LogsModel.UpdateLogs;
using CleanArchitecture.Application.Schedules.CreateSchedule;
using CleanArchitecture.Application.Schedules.DeleteSchedule;
using CleanArchitecture.Application.Schedules.GetSchedule;
using CleanArchitecture.Application.Schedules.GetScheduleByCustomer;
using CleanArchitecture.Application.Schedules.GetScheduleByTittle;
using CleanArchitecture.Application.Schedules.UpdateSchedule;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace CleanArchitecture.Api.Controllers
{
    [Authorize]
    public class ScheduleController : ControllerBase
    {
        private readonly ISender _mediator;

        public ScheduleController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet("api/schedule")]
        [ProducesResponseType(typeof(PagedResults<ScheduleDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PagedResults<ScheduleDTO>>> GetSchedule([FromQuery] GetScheduleCommand command, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
        [HttpGet("api/schedule/{tittle}")]
        [ProducesResponseType(typeof(ScheduleDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ScheduleDTO>> GetScheduleById(string tittle, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new GetScheduleByTittleCommand { Tittle = tittle }, cancellationToken);

            if (result != null)
            {
                return Ok(result);
            }
            return NotFound($"Schedule with Tittle {tittle} not found");
        }

        [HttpGet("api/schedule/customer/{customerId}")]
        [ProducesResponseType(typeof(ScheduleDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ScheduleDTO>> GetScheduleByCustomerId( int customerId, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new GetScheduleByCustomerCommand { id = customerId }, cancellationToken);

            if (result != null)
            {
                return Ok(result);
            }
            return NotFound($"Schedule with id {customerId} not found");
        }
        //[HttpPost("schedule")]
        //[Produces(MediaTypeNames.Application.Json)]
        //[ProducesResponseType(typeof(JsonResponse<Schedule>), StatusCodes.Status201Created)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<JsonResponse<Schedule>>> CreateSchedule(
        //[FromBody] CreateScheduleCommand command,
        //CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(command, cancellationToken);
        //    //return CreatedAtAction(nameof(GetOrderById), new { id = result }, new JsonResponse<Guid>(result));
        //    return CreatedAtAction(nameof(CreateSchedule), new JsonResponse<Schedule>(result));
        //}
        [HttpPost("schedule")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(JsonResponse<Schedule>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateSchedule(
        [FromBody] CreateScheduleCommand command,
        CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //[HttpDelete("api/schedule/{Tittle}")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult> DeleteSchedule([FromRoute] string Tittle, CancellationToken cancellationToken = default)
        //{
        //    await _mediator.Send(new DeleteScheduleCommand(tittle: Tittle), cancellationToken);
        //    return Ok();
        //}
        [HttpDelete("api/schedule/{Tittle}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteSchedule([FromRoute] string Tittle, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(new DeleteScheduleCommand(tittle: Tittle), cancellationToken);

                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //[HttpPut("api/schedule")]
        //[ProducesResponseType(StatusCodes.Status204NoContent)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult> UpdateSchedule(
        //    [FromBody] UpdateScheduleCommand command,
        //    CancellationToken cancellationToken = default)
        //{
        //    try
        //    {
        //        await _mediator.Send(command, cancellationToken);
        //        return NoContent();
        //    }
        //    catch (NotFoundException ex)
        //    {
        //        return NotFound(ex.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing the request.");
        //    }
        //}
        [HttpPut("api/schedule")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateSchedule(
        [FromBody] UpdateScheduleCommand command,
        CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
