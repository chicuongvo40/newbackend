﻿using CleanArchitecture.Api.Controllers.ResponseTypes;
using CleanArchitecture.Application.Sources.CreateSource;
using CleanArchitecture.Application.Sources.DeleteSource;
using CleanArchitecture.Application.Sources.UpdateSource;
using CleanArchitecture.Application.Survey.CreateSurvey;
using CleanArchitecture.Application.Survey.DeleteSurvey;
using CleanArchitecture.Application.Survey.GetSurvey;
using CleanArchitecture.Application.Survey.GetSurveyById;
using CleanArchitecture.Application.Survey.UpdateSurvey;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace CleanArchitecture.Api.Controllers
{
    //[Authorize]
    public class SurveyController : ControllerBase
    {
        private readonly ISender _mediator;

        public SurveyController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet("api/Survey")]
        [ProducesResponseType(typeof(PagedResults<SurveyDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PagedResults<SurveyDTO>>> GetSurvey([FromQuery] GetSurveyCommand command, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
        [HttpGet("api/Survey/customerId")]
        [ProducesResponseType(typeof(SurveyDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<SurveyDTO>> GetSurveyByCustomerId(int Id, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new GetSurveyByCustomerIdCommand { CustomerId = Id }, cancellationToken);

            if (result != null)
            {
                return Ok(result);
            }
            return NotFound($"Survey with ID {Id} not found");
        }
        //[HttpPost("Survey")]
        //[Produces(MediaTypeNames.Application.Json)]
        //[ProducesResponseType(typeof(JsonResponse<Survey>), StatusCodes.Status201Created)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<JsonResponse<Survey>>> CreateSurvey(
        //[FromBody] CreateSurveyCommand command,
        //CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(command, cancellationToken);
        //    //return CreatedAtAction(nameof(GetOrderById), new { id = result }, new JsonResponse<Guid>(result));
        //    return CreatedAtAction(nameof(CreateSurvey), new JsonResponse<Survey>(result));
        //}
        [HttpPost("Survey")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(JsonResponse<Survey>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateSurvey(
        [FromBody] CreateSurveyCommand command,
        CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return Ok(new { Status = "Success", Message = "Survey Created Successfully!" });
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //[HttpDelete("api/Survey/{id}")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult> DeleteSurvey([FromRoute] int id, CancellationToken cancellationToken = default)
        //{
        //    await _mediator.Send(new DeleteSurveyCommand(surveyId: id), cancellationToken);
        //    return Ok();
        //}
        [HttpDelete("api/Survey/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteSurvey([FromRoute] int id, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(new DeleteSurveyCommand(surveyId: id), cancellationToken);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //[HttpPut("api/Survey")]
        //[ProducesResponseType(StatusCodes.Status204NoContent)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult> UpdateSurvey(
        //    [FromBody] UpdateSurveyCommand command,
        //    CancellationToken cancellationToken = default)
        //{
        //    try
        //    {
        //        await _mediator.Send(command, cancellationToken);
        //        return NoContent();
        //    }
        //    catch (NotFoundException ex)
        //    {
        //        return NotFound(ex.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing the request.");
        //    }
        //}
        [HttpPut("api/Survey")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateSurvey(
        [FromBody] UpdateSurveyCommand command,
        CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
