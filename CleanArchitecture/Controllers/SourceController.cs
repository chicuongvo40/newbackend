﻿using CleanArchitecture.Api.Controllers.ResponseTypes;
using CleanArchitecture.Application.OdsService.CreateOds;
using CleanArchitecture.Application.OdsService.DeleteOds;
using CleanArchitecture.Application.OdsService.UpdateOds;
using CleanArchitecture.Application.Sources.CreateSource;
using CleanArchitecture.Application.Sources.DeleteSource;
using CleanArchitecture.Application.Sources.GetSource;
using CleanArchitecture.Application.Sources.GetSourceById;
using CleanArchitecture.Application.Sources.UpdateSource;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace CleanArchitecture.Api.Controllers
{
    [Authorize]
    public class SourceController : ControllerBase
    {
        private readonly ISender _mediator;

        public SourceController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet("api/source")]
        [ProducesResponseType(typeof(PagedResults<SourceDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PagedResults<SourceDTO>>> GetSource([FromQuery] GetSourceCommand command, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
        [HttpGet("api/source/{id}")]
        [ProducesResponseType(typeof(SourceDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<SourceDTO>> GetSourceById(int id, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new GetSourceByIdCommand { SourceId = id }, cancellationToken);

            if (result != null)
            {
                return Ok(result);
            }
            return NotFound($"Branch with ID {id} not found");
        }
       // [HttpPost("source")]
       // [Produces(MediaTypeNames.Application.Json)]
       // [ProducesResponseType(typeof(JsonResponse<Source>), StatusCodes.Status201Created)]
       // [ProducesResponseType(StatusCodes.Status400BadRequest)]
       // [ProducesResponseType(StatusCodes.Status401Unauthorized)]
       // [ProducesResponseType(StatusCodes.Status403Forbidden)]
       // [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
       // public async Task<ActionResult<JsonResponse<Source>>> CreateSource(
       //[FromBody] CreateSourceCommand command,
       //CancellationToken cancellationToken = default)
       // {
       //     var result = await _mediator.Send(command, cancellationToken);
       //     return CreatedAtAction(nameof(CreateSource), new JsonResponse<Source>(result));
       // }
        [HttpPost("source")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(JsonResponse<Source>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateSource(
        [FromBody] CreateSourceCommand command,
        CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return Ok(new { Status = "Success", Message = "Source Created Successfully!" });
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //[HttpDelete("api/source/{id}")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult> DeleteSource([FromRoute] int id, CancellationToken cancellationToken = default)
        //{
        //    await _mediator.Send(new DeleteSourceCommand(sourceId: id), cancellationToken);
        //    return Ok();
        //}
        [HttpDelete("api/source/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteSource([FromRoute] int id, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(new DeleteSourceCommand(sourceId: id), cancellationToken);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //[HttpPut("api/source")]
        //[ProducesResponseType(StatusCodes.Status204NoContent)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult> UpdateBranch(
        //    [FromBody] UpdateSourceCommand command,
        //    CancellationToken cancellationToken = default)
        //{
        //    try
        //    {
        //        await _mediator.Send(command, cancellationToken);
        //        return NoContent();
        //    }
        //    catch (NotFoundException ex)
        //    {
        //        return NotFound(ex.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing the request.");
        //    }
        //}
        [HttpPut("api/source")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateSource(
        [FromBody] UpdateSourceCommand command,
        CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
