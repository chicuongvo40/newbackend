﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using CleanArchitecture.Api.Models;

[ApiController]
[Route("api/cloudfone")]
public class CloudFoneApiController : ControllerBase
{
    private static readonly HttpClient client = new HttpClient();

    [HttpPost("get-call-history")]
    public async Task<IActionResult> GetCallHistory([FromBody] CallHistoryRequestDTO _request)
    {
        try
        {
            // Define the request payload
            var requestPayload = new
            {
                ServiceName = "CF-PBX0001442",
                AuthUser = "ODS000953",
                AuthKey = "d8e8990b-60c6-4b50-9fac-f766078a3f19",
                TypeGet = "4",
                HideFWOut = 1,
                DateStart = _request.DateStart.ToString(),
                DateEnd = _request.DateEnd.ToString(),
                IsSort = "",
                CallNum = "",
                ReceiveNum = "",
                Key = _request.Key.ToString(),
                PageIndex = 1,
                PageSize = 200
            };

            // Convert the request payload to JSON
            var jsonRequest = JsonSerializer.Serialize(requestPayload);

            // Set up the HTTP content
            var content = new StringContent(jsonRequest, System.Text.Encoding.UTF8, "application/json");

            // Make a POST request to the CloudFone API
            var response = await client.PostAsync("https://api.cloudfone.vn/api/CloudFone/GetCallHistory", content);

            // Check if the request was successful
            if (response.IsSuccessStatusCode)
            {
                // Read and parse the response content
                var jsonResponse = await response.Content.ReadAsStringAsync();
                var deserializedResponse = JsonSerializer.Deserialize<CallHistoryResponse>(jsonResponse);

                // Return a 200 OK response with the parsed JSON
                return Ok(deserializedResponse);
            }
            else
            {
                // Return the status code and the reason phrase if the request was not successful
                return StatusCode((int)response.StatusCode, response.ReasonPhrase);
            }
        }
        catch (Exception ex)
        {
            // Return a 500 Internal Server Error with the exception message
            return StatusCode(500, ex.Message);
        }
    }
}