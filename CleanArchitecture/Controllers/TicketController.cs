﻿using CleanArchitecture.Api.Controllers.ResponseTypes;
using CleanArchitecture.Application.Tickets.CreateTicket;
using CleanArchitecture.Application.Tickets.DeleteTicket;
using CleanArchitecture.Application.Tickets.GetTicketByCreatedCustomer;
using CleanArchitecture.Application.Tickets.GetTicketByCustomer;
using CleanArchitecture.Application.Tickets.GetTicketById;
using CleanArchitecture.Application.Tickets.GetTicketByUserId;
using CleanArchitecture.Application.Tickets.GetTicketName;
using CleanArchitecture.Application.Tickets.GetTicketNameById;
using CleanArchitecture.Application.Tickets.GetTicketNameByPhone;
using CleanArchitecture.Application.Tickets.GetTicketWithTag;
using CleanArchitecture.Application.Tickets.SearchTicketDate;
using CleanArchitecture.Application.Tickets.UpdateTicket;
using CleanArchitecture.Application.Tickets.UpdateTicketAcceptCustomer;
using CleanArchitecture.Application.Tickets.UpdateTicketUserStatus;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace CleanArchitecture.Api.Controllers
{
    [Authorize]
    public class TicketController : ControllerBase
    {
        private readonly ISender _mediator;

        public TicketController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet("api/ticket")]
        [ProducesResponseType(typeof(PagedResults<TicketName>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PagedResults<TicketName>>> GetTicketName([FromQuery] GetTicketNameCommand command, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
        [HttpGet("api/ticket/{id}")]
        [ProducesResponseType(typeof(TicketName), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<TicketName>> GetTicketNameById(int id, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new GetTicketNameByIdCommand { TicketId = id }, cancellationToken);


            if (result != null)
            {
                return Ok(result);
            }
            return BadRequest($"ticket with ID {id} not found");
        }


        [HttpGet("api/ticket/id/{id}")]
        [ProducesResponseType(typeof(TicketName), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<TicketName>> GetTicketNameIdById(int id, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new GetTicketByIdCommand { TicketId = id }, cancellationToken);


            if (result != null)
            {
                return Ok(result);
            }
            return BadRequest($"ticket with ID {id} not found");
        }
        [HttpGet("api/ticket/phone")]
        [ProducesResponseType(typeof(List<TicketName>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<TicketName>>> GetTicketNameByPhone(string phone, CancellationToken cancellationToken = default)
        {
            var command = new GetTicketNameByPhoneCommand { PhoneNumber = phone };
            var result = await _mediator.Send(command, cancellationToken);

            if (result != null && result.Count > 0)
            {
                return Ok(result);
            }

            return BadRequest($"No users found");
        }

        [HttpGet("api/ticket/createdcustomer")]
        [ProducesResponseType(typeof(List<TicketName>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<TicketName>>> GetTicketNameByCreatedCustomer(CancellationToken cancellationToken = default)
        {
            var command = new GetTicketByCreatedCustomerCommand();
            var result = await _mediator.Send(command, cancellationToken);

            return Ok(result);



        }

        [HttpGet("api/ticket/CustomerId/{id}")]
        [ProducesResponseType(typeof(List<TicketName>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<TicketName>>> GetTicketByCustomerId(int id, CancellationToken cancellationToken = default)
        {
            var command = new GetTicketByCustomerCommand { UserId = id };
            var result = await _mediator.Send(command, cancellationToken);

            if (result != null && result.Count > 0)
            {
                return Ok(result);
            }

            return BadRequest($"No users found");
        }

        [HttpGet("api/ticket/UserId")]
        [ProducesResponseType(typeof(List<TicketName>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<TicketName>>> GetTicketByUserId(int phone, CancellationToken cancellationToken = default)
        {
            var command = new GetTicketByUserIdCommand { UserId = phone };
            var result = await _mediator.Send(command, cancellationToken);

            if (result != null && result.Count > 0)
            {
                return Ok(result);
            }

            return BadRequest($"No users found");
        }

        [HttpGet("api/ticket/statusId")]
        [ProducesResponseType(typeof(TicketWithTagDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<TicketWithTagDTO>> GetTicketWithTag(GetTicketWithTagCommand command, CancellationToken cancellationToken = default)
        {

            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
        //[HttpGet("api/allticket")]
        //[ProducesResponseType(typeof(PagedResults<Ticket>), StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<PagedResults<Ticket>>> GetALLTicket([FromQuery] GetAllTicketCommand command, CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(command, cancellationToken);
        //    return Ok(result);
        //}
        //[HttpGet("api/ticket/{id}")]
        //[ProducesResponseType(typeof(TicketDTO), StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<TicketDTO>> GetTicketById(int id, CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(new GetTicketByIdCommand { TicketId = id }, cancellationToken);

        //    if (result != null)
        //    {
        //        return Ok(result);
        //    }
        //    return NotFound($"ticket with ID {id} not found");
        //}
        //[HttpGet("api/ticket/Status")]
        //[ProducesResponseType(typeof(TicketDTO), StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<TicketDTO>> GetTicketByStatus(int id, CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(new GetTicketByStatusCommand { TicketStatusId = id }, cancellationToken);

        //    if (result != null)
        //    {
        //        return Ok(result);
        //    }
        //    return NotFound($"ticket with ID {id} not found");
        //}
        //[HttpGet("api/ticket/SearchDate")]
        //[ProducesResponseType(typeof(TicketDTO), StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<TicketDTO>> SreachTicketDate(DateTime date, CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(new SearchTicketDateCommand { CreatedDate = date }, cancellationToken);

        //    if (result != null)
        //    {
        //        return Ok(result);
        //    }
        //    return NotFound($"ticket with {date} not found");
        //}
        //[HttpPost("ticket")]
        //[Produces(MediaTypeNames.Application.Json)]
        //[ProducesResponseType(typeof(JsonResponse<Ticket>), StatusCodes.Status201Created)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<JsonResponse<Ticket>>> CreateTicket(
        //  [FromBody] CreateTicketCommand command,
        //  CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(command, cancellationToken);
        //    //return CreatedAtAction(nameof(GetOrderById), new { id = result }, new JsonResponse<Guid>(result));
        //    return CreatedAtAction(nameof(CreateTicket), new JsonResponse<Ticket>(result));
        //}
        [HttpPost("ticket")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(JsonResponse<Ticket>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateTicket(
        [FromBody] CreateTicketCommand command,
        CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //[HttpDelete("api/ticket/{id}")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult> DeleteTicket([FromRoute] int id, CancellationToken cancellationToken = default)
        //{
        //    await _mediator.Send(new DeleteTicketCommand(_ticketId: id), cancellationToken);
        //    return Ok();
        //}
        [HttpDelete("api/ticket/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteTicket([FromRoute] int id, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(new DeleteTicketCommand(_ticketId: id), cancellationToken);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //[HttpPut("api/ticket")]
        //[ProducesResponseType(StatusCodes.Status204NoContent)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult> UpdateTicket(
        //   [FromBody] UpdateTicketCommand command,
        //   CancellationToken cancellationToken = default)
        //{
        //    try
        //    {
        //        await _mediator.Send(command, cancellationToken);
        //        return NoContent();
        //    }
        //    catch (NotFoundException ex)
        //    {
        //        return NotFound(ex.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing the request.");
        //    }
        //}

        //[HttpGet("api/ticket")]
        //[ProducesResponseType(typeof(PagedResults<Ticket>), StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<PagedResults<Ticket>>> GetTicket([FromQuery] GetTicketCommand command, CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(command, cancellationToken);
        //    return Ok(result);
        //}

        [HttpPost("ticketbycustomer")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(JsonResponse<Ticket>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateTicketByCustomer(
       [FromBody] CreateTicketByCustomerCommand command,
       CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return Ok(new { Status = "Success", Message = "Ticket Created Successfully!" });
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }




        [HttpPut("api/ticket")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateTicket(
        [FromBody] UpdateTicketCommand command,
        CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpPut("api/ticketuser")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateTicketUserStatus(
       [FromBody] UpdateTicketUserStatusCommand command,
       CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("api/ticketacceptedcustomer")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateTicketAcceptedCustomer(
      [FromBody] UpdateTicketAcceptCustomerCommand command,
      CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
