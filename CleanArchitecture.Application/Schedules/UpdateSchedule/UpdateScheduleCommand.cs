﻿using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;

namespace CleanArchitecture.Application.Schedules.UpdateSchedule
{
    public class UpdateScheduleCommand : IRequest<Unit>, ICommand
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int StaffId { get; set; }
        public string Tittle { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime MeetTime { get; set; }
        public DateTime LastEditedTime { get; set; }
    }
}
