﻿using CleanArchitecture.Application.Branchs.UpdateBrach;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Schedules.UpdateSchedule
{
    public class UpdateScheduleCommandHandler : IRequestHandler<UpdateScheduleCommand, Unit>
    {
        private readonly IScheduleRepositories _scheduleRepository;
        public UpdateScheduleCommandHandler(IScheduleRepositories scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;
        }
        public async Task<Unit> Handle(UpdateScheduleCommand request, CancellationToken cancellationToken)
        {
            var existingSchedule = await _scheduleRepository.SearchByTittle(request.Tittle, cancellationToken);

            if (existingSchedule == null)
            {
                throw new NotFoundException($"Schedule with Title '{request.Tittle}' not found");
            }

            // Kiểm tra nếu danh sách có ít nhất một phần tử
            if (existingSchedule.Count > 0)
            {
                // Lấy phần tử đầu tiên trong danh sách để cập nhật
                var scheduleToUpdate = existingSchedule[0];

                // Cập nhật thông tin của lịch từ dữ liệu trong request
                scheduleToUpdate.Tittle = request.Tittle;
                scheduleToUpdate.CustomerId = request.CustomerId;
                scheduleToUpdate.StaffId = request.StaffId;
                scheduleToUpdate.Status = request.Status;
                scheduleToUpdate.Note = request.Note;
                scheduleToUpdate.CreatedDate = request.CreatedDate;
                scheduleToUpdate.MeetTime = request.MeetTime;
                scheduleToUpdate.LastEditedTime = request.LastEditedTime;

                // Lưu các thay đổi vào cơ sở dữ liệu
                _scheduleRepository.Update(scheduleToUpdate);
                await _scheduleRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            else
            {
                throw new NotFoundException($"Schedule with Title '{request.Tittle}' not found");
            }

            return Unit.Value;
        }
    }
}
