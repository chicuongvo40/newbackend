﻿using AutoMapper;
using CleanArchitecture.Application.Schedules.GetSchedule;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Schedules.GetScheduleByCustomer
{
    public class GetScheduleByCustomerCommandHandler : IRequestHandler<GetScheduleByCustomerCommand, ScheduleDTO>
    {
        private readonly IScheduleRepositories _scheduleRepository;
        private readonly IMapper _mapper;
      

        public GetScheduleByCustomerCommandHandler(IScheduleRepositories scheduleRepository, IMapper mapper)
        {
            _scheduleRepository = scheduleRepository;
            _mapper = mapper;
        }
        public async Task<ScheduleDTO> Handle(GetScheduleByCustomerCommand request, CancellationToken cancellationToken)
        { 
            Console.WriteLine("Có gọi tới đây");
            var schedule = await _scheduleRepository.FindByCustomerIdAsync(request.id);
            Debug.WriteLine(schedule[0]  + "AAAAAAA");
            var op =  schedule[schedule.Count - 1];
            var ops = _mapper.Map<ScheduleDTO>(op);
            return ops;
        }
    }
}
