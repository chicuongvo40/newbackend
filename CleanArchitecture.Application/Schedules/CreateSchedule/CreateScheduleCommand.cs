﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Schedules.CreateSchedule
{
    public class CreateScheduleCommand : IRequest<Schedule>, ICommand
    {
        public string CustomerId { get; set; }

        public int StaffId { get; set; }

        public string Tittle { get; set; }

        public string Status { get; set; }

        public string Note { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime MeetTime { get; set; }

        public DateTime LastEditedTime { get; set; }
    }
}
