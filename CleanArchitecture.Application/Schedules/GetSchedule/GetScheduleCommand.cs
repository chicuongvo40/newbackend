﻿using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
namespace CleanArchitecture.Application.Schedules.GetSchedule
{
    public class GetScheduleCommand : IRequest<PagedResults<ScheduleDTO>>, ICommand
    {
        public int page { get; set; }

        public int size { get; set; }
    }
}
