﻿using AutoMapper;
using CleanArchitecture.Application.Branchs.GetBranch;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Schedules.GetSchedule
{
    public class GetScheduleCommandHandler : IRequestHandler<GetScheduleCommand, PagedResults<ScheduleDTO>>
    {
        private readonly IScheduleRepositories _scheduleRepository;
        private readonly IMapper _mapper;

        public GetScheduleCommandHandler(IScheduleRepositories scheduleRepository, IMapper mapper)
        {
            _scheduleRepository = scheduleRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<ScheduleDTO>> Handle(GetScheduleCommand request, CancellationToken cancellationToken)
        {
            var schedule = await _scheduleRepository.FindAllAsync();
            var scheduleDTOs = _mapper.Map<List<ScheduleDTO>>(schedule);
            var op = PageHelper<ScheduleDTO>.Paging(scheduleDTOs, request.page, request.size);
            return op;
        }
    }
}
