﻿using CleanArchitecture.Application.Branchs.DeleteBranch;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Schedules.DeleteSchedule
{
    public class DeleteScheduleCommandHandler : IRequestHandler<DeleteScheduleCommand, Unit>
    {
        private readonly IScheduleRepositories _scheduleRepository;

        public DeleteScheduleCommandHandler(IScheduleRepositories scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;
        }
        public async Task<Unit> Handle(DeleteScheduleCommand request, CancellationToken cancellationToken)
        {
            var schedules = await _scheduleRepository.SearchByTittle(request.Tittle, cancellationToken);

            // Kiểm tra xem có lịch trình nào được tìm thấy không
            if (schedules == null || !schedules.Any())
            {
                throw new NotFoundException("No schedules found with the given title.");
            }

            // Xóa từng lịch trình
            foreach (var schedule in schedules)
            {
                _scheduleRepository.Remove(schedule);
            }

            // Lưu thay đổi vào cơ sở dữ liệu
            await _scheduleRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}

