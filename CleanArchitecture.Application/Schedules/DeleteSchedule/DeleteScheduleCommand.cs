﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
namespace CleanArchitecture.Application.Schedules.DeleteSchedule
{
    public class DeleteScheduleCommand : IRequest<Unit>, ICommand
    {
        public DeleteScheduleCommand(string tittle)
        {
            Tittle = tittle;
        }
        public string Tittle { get; set; }
    }
}
