﻿using AutoMapper;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CallHistorys.GetCallHistoryByDirection
{
    public class GetCallHistoryByDirectionQueryHandler :
        IRequestHandler<GetCallHistoryByDirectionQuery, List<CallHistoryDTO>>
    {
        private readonly ICallHistoryRepositories _callHistoryRepository;
        private readonly IMapper _mapper;

        public GetCallHistoryByDirectionQueryHandler(ICallHistoryRepositories callHistoryRepository, IMapper mapper)
        {
            _callHistoryRepository = callHistoryRepository;
            _mapper = mapper;
        }

        public async Task<List<CallHistoryDTO>> Handle(GetCallHistoryByDirectionQuery request, CancellationToken cancellationToken)
        {
            var direction = request.Direction.ToString(); // Convert enum thành chuỗi
            var callHistoryList = await _callHistoryRepository.SearchByDirection(direction, cancellationToken);
            return _mapper.Map<List<CallHistoryDTO>>(callHistoryList);
        }
    }
}
