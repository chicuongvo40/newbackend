﻿using AutoMapper;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CallHistorys.GetAllCallHistory
{
    public class GetAllCallHistoryCommandHandler : IRequestHandler<GetAllCallHistoryCommand, PagedResults<CallHistoryDTO>>
    {
        private readonly ICallHistoryRepositories _questionRepo;
        private readonly IMapper _mapper;
        public GetAllCallHistoryCommandHandler(ICallHistoryRepositories questionRepo, IMapper mapper)
        {
            _questionRepo = questionRepo;
            _mapper = mapper;
        }
        public async Task<PagedResults<CallHistoryDTO>> Handle(GetAllCallHistoryCommand request, CancellationToken cancellationToken)
        {
            var callhistory = await _questionRepo.FindAllAsync();
            var calcallhistoryDTO = _mapper.Map<List<CallHistoryDTO>>(callhistory);
            var op = PageHelper<CallHistoryDTO>.Paging(calcallhistoryDTO, request.page, request.size);
            return op;
        }
    }
}
