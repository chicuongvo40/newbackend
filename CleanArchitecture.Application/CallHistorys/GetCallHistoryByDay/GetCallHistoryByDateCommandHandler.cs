﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CallHistorys.GetCallHistoryByDay
{
    public class GetCallHistoryByDateCommandHandler : IRequestHandler<GetCallHistoryByDateCommand, List<CallHistoryDTO>>
    {
        private readonly ICallHistoryRepositories _callHistoryRepository;
        private readonly IMapper _mapper;
        public GetCallHistoryByDateCommandHandler(ICallHistoryRepositories callHistoryRepository, IMapper mapper)
        {
            _callHistoryRepository = callHistoryRepository;
            _mapper = mapper;
        }

        public async Task<List<CallHistoryDTO>> Handle(GetCallHistoryByDateCommand request, CancellationToken cancellationToken)
        {
            var dateCall = request.DateCall;

            var callHistoryList = await _callHistoryRepository.SearchDayAsync(dateCall, cancellationToken);

            if (callHistoryList != null && callHistoryList.Any())
            {
                // If needed, you can use AutoMapper to map entities to DTOs here
                 var callHistoryDTOList = _mapper.Map<List<CallHistoryDTO>>(callHistoryList);
                 return callHistoryDTOList;

                //return callHistoryDTOList;
            }

            throw new NotFoundException($"Call history for date {dateCall} not found");
        }
    }
}

