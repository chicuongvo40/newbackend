﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CallHistorys.CreateCallHistory
{
    public class CreateCallHistoryCommandHandler : IRequestHandler<CreateCallHistoryCommand, CallHistory>
    {
        private readonly ICallHistoryRepositories _callRepo;
        public CreateCallHistoryCommandHandler(ICallHistoryRepositories callRepo)
        {
            _callRepo = callRepo;
        }
        public async Task<CallHistory> Handle(CreateCallHistoryCommand request, CancellationToken cancellationToken)
        {
            var call = new CallHistory(
               request.CallNumber,
               request.UserId,
               request.CustomerId,
               request.RealTimeCall,
               request.StatusCall,
               request.DateCall,
               request.RecordLink,
               request.Direction,
               request.TotalTimeCall,
               request.Code
                );
            //await _callRepo.CreateCallHistorySP(call);
            _callRepo.Add(call);
            await _callRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return call;
        }
    }
}
