﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CallHistorys.GetCallHistoryByPhone
{
    public class GetCallHistoryByPhoneCommand : IRequest<List<CallHistoryDTO>>, ICommand
    {
        public string Phone { get; set; }
    }
}
