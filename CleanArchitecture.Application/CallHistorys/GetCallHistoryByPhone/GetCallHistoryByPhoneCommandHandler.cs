﻿using AutoMapper;
using CleanArchitecture.Application.CallHistorys.GetCallHistoryByCustomerId;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CallHistorys.GetCallHistoryByPhone
{
    public class GetCallHistoryByPhoneCommandHandler : IRequestHandler<GetCallHistoryByPhoneCommand, List<CallHistoryDTO>>
    {
        private readonly ICallHistoryRepositories _callHistoryRepository;
        private readonly IMapper _mapper;

        public GetCallHistoryByPhoneCommandHandler(ICallHistoryRepositories callHistoryRepository, IMapper mapper)
        {
            _callHistoryRepository = callHistoryRepository;
            _mapper = mapper;
        }

        public async Task<List<CallHistoryDTO>> Handle(GetCallHistoryByPhoneCommand request, CancellationToken cancellationToken)
        {
            var phone = request.Phone;
            var call = await _callHistoryRepository.FindByPhoneAsync(phone, cancellationToken);

            if (call != null && call.Count > 0)
            {
                var userDTOs = _mapper.Map<List<CallHistoryDTO>>(call);
                return userDTOs;
            }

            throw new NotFoundException($"No Call found");
        }
    }
}