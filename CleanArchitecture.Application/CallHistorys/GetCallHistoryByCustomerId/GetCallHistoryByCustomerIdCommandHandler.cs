﻿using AutoMapper;
using CleanArchitecture.Application.Users.GetUserByRoleId;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CallHistorys.GetCallHistoryByCustomerId
{
    public class GetCallHistoryByCustomerIdCommandHandler : IRequestHandler<GetCallHistoryByCustomerIdCommand, List<CallHistoryDTO>>
    {
        private readonly ICallHistoryRepositories _callHistoryRepository;
        private readonly IMapper _mapper;

        public GetCallHistoryByCustomerIdCommandHandler(ICallHistoryRepositories callHistoryRepository, IMapper mapper)
        {
            _callHistoryRepository = callHistoryRepository;
            _mapper = mapper;
        }

        public async Task<List<CallHistoryDTO>> Handle(GetCallHistoryByCustomerIdCommand request, CancellationToken cancellationToken)
        {
            var customerId = request.CustomerId;
            var call = await _callHistoryRepository.FindByCustomerIdAsync(customerId, cancellationToken);

            if (call != null && call.Count > 0)
            {
                var userDTOs = _mapper.Map<List<CallHistoryDTO>>(call);
                return userDTOs;
            }

            throw new NotFoundException($"No users found with Role ID {customerId}");
        }
    }
}
