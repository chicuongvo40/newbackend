﻿using AutoMapper;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CallHistorys.GetCallHistory
{
    public class GetCallHistoryCommandHandler : IRequestHandler<GetCallHistoryCommand, PagedResults<CallHistoryDTO>>
    {
        private readonly ICallHistoryRepositories _questionRepo;
        private readonly IMapper _mapper;
        public GetCallHistoryCommandHandler(ICallHistoryRepositories questionRepo, IMapper mapper)
        {
            _questionRepo = questionRepo;
            _mapper = mapper;
        }
        public async Task<PagedResults<CallHistoryDTO>> Handle(GetCallHistoryCommand request, CancellationToken cancellationToken)
        {
            var callhistory = await _questionRepo.GetCustomerByUserId(request.id);
            var calcallhistoryDTO = _mapper.Map<List<CallHistoryDTO>>(callhistory);
            var op = PageHelper<CallHistoryDTO>.Paging(calcallhistoryDTO, request.page, request.size);
            return op;
        }
    }
}
