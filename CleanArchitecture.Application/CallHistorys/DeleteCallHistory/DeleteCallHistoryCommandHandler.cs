﻿using CleanArchitecture.Application.Branchs.DeleteBranch;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CallHistorys.DeleteCallHistory
{
    public class DeleteCallHistoryCommandHandler : IRequestHandler<DeleteCallHistoryCommand, Unit>
    {
        private readonly ICallHistoryRepositories _callRepository;


        public DeleteCallHistoryCommandHandler(ICallHistoryRepositories callRepository)
        {
            _callRepository = callRepository;
        }
        public async Task<Unit> Handle(DeleteCallHistoryCommand request, CancellationToken cancellationToken)
        {
            // await _callRepository.DeleteCallHistorySP(request.Id); 
            var call = await _callRepository.FindByIdAsync(request.Id, cancellationToken);

            if (call == null)
            {
                throw new NotFoundException("CallHistory not found.");
            }
                _callRepository.Remove(call);
                await _callRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            
            return Unit.Value;
            
        }
    }
}
