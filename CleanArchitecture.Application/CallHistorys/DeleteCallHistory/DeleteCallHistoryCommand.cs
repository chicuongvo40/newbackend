﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.CallHistorys.DeleteCallHistory
{
    public class DeleteCallHistoryCommand : IRequest<Unit>, ICommand
    {
        public DeleteCallHistoryCommand(int callid)
        {
            Id = callid;
        }
        public int Id { get; set; }
    }
}
