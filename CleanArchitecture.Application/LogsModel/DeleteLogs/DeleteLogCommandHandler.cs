﻿using CleanArchitecture.Application.Branchs.DeleteBranch;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.LogsModel.DeleteLogs
{
    public class DeleteLogCommandHandler : IRequestHandler<DeleteLogCommand, Unit>
    {
        private readonly ILogRepository _logRepository;

        public DeleteLogCommandHandler(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }
        public async Task<Unit> Handle(DeleteLogCommand request, CancellationToken cancellationToken)
        {
            //await _logRepository.DeleteLogSP(request.Id); 
            var log = await _logRepository.FindByIdAsync(request.Id, cancellationToken);

            if (log == null)
            {
                throw new NotFoundException("Log not found.");
            }
                _logRepository.Remove(log);
                await _logRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            
            return Unit.Value;
        }
    }
}
