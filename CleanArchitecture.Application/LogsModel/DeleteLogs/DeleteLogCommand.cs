﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
namespace CleanArchitecture.Application.LogsModel.DeleteLogs
{
    public class DeleteLogCommand : IRequest<Unit>, ICommand
    {
        public DeleteLogCommand(int logid)
        {
            Id = logid;
        }
        public int Id { get; set; }
    }
}
