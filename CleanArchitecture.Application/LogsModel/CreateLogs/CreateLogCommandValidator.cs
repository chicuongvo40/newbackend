﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.LogsModel.CreateLogs
{
    public class CreateLogCommandValidator : AbstractValidator<CreateLogCommand>
    {
        public CreateLogCommandValidator()
        {
            RuleFor(command => command.Note)
                .MaximumLength(1000).WithMessage("Note must not exceed 1000 characters.");

            RuleFor(command => command.CreatedDate)
                .NotEmpty().WithMessage("Created date is required");
        }
    }
}
