﻿using CleanArchitecture.Application.Branchs.CreateBranch;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.LogsModel.CreateLogs
{
    public class CreateLogCommandHandler : IRequestHandler<CreateLogCommand, Log>
    {
        private readonly ILogRepository _logRepository;
        public CreateLogCommandHandler(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }
        public async Task<Log> Handle(CreateLogCommand request, CancellationToken cancellationToken)
        {
            var _log = new Log(
              request.TicketId,
               request.Note,
               request.CreatedDate,
               request.Code,
               request.ImgUrl
                );
            //await _logRepository.CreateLogSP(_log);
            _logRepository.Add(_log);
            await _logRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return _log;
        }
    }
}
