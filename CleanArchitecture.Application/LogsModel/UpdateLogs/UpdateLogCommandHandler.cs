﻿using CleanArchitecture.Application.Branchs.UpdateBrach;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.LogsModel.UpdateLogs
{
    public class UpdateLogCommandHandler : IRequestHandler<UpdateLogCommand, Unit>
    {
        private readonly ILogRepository _logRepository;
        public UpdateLogCommandHandler(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }
        public async Task<Unit> Handle(UpdateLogCommand request, CancellationToken cancellationToken)
        {
            var log = await _logRepository.FindByIdAsync(request.Id, cancellationToken);

            if (log == null)
            {
                throw new NotFoundException("Log not found.");
            }
            log.TicketId = request.TicketId;
            log.Note = request.Note;
            log.CreatedDate = request.CreatedDate;
            log.Code = request.Code;
            log.ImgUrl = request.ImgUrl;
            //await _logRepository.UpdateLogSP(bra);
            _logRepository.Update(log);
            await _logRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}