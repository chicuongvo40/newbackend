﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Survey.DeleteSurvey
{
    public class DeleteSurveyCommand : IRequest<Unit>, ICommand
    {
        public DeleteSurveyCommand(int surveyId)
        {
            Id = surveyId;
        }
        public int Id { get; set; }
    }
}

