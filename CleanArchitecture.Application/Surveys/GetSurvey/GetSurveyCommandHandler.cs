﻿using AutoMapper;
using CleanArchitecture.Application.Branchs.GetBranch;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Survey.GetSurvey
{
    public class GetSurveyCommandHandler : IRequestHandler<GetSurveyCommand, PagedResults<SurveyDTO>>
    {
        private readonly ISurveyRepositories _surveyRepository;
        private readonly IMapper _mapper;

        public GetSurveyCommandHandler(ISurveyRepositories surveyRepository, IMapper mapper)
        {
            _surveyRepository = surveyRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<SurveyDTO>> Handle(GetSurveyCommand request, CancellationToken cancellationToken)
        {
            var survey = await _surveyRepository.FindAllAsync();
            var surveyDTOs = _mapper.Map<List<SurveyDTO>>(survey);
            var op = PageHelper<SurveyDTO>.Paging(surveyDTOs, request.page, request.size);
            return op;
        }
    }
}
