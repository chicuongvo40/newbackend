﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Survey.GetSurveyById
{
    public class GetSurveyByCustomerIdCommandHandler : IRequestHandler<GetSurveyByCustomerIdCommand, List<SurveyDTO>>
    {
        private readonly ISurveyRepositories _surveyRepo;
        private readonly IMapper _mapper;
        public GetSurveyByCustomerIdCommandHandler(ISurveyRepositories surveyRepo, IMapper mapper)
        {
            _surveyRepo = surveyRepo;
            _mapper = mapper;
        }
        public async Task<List<SurveyDTO>> Handle(GetSurveyByCustomerIdCommand request, CancellationToken cancellationToken)
        {
            var customerId = request.CustomerId;

            var survey = await _surveyRepo.FindByCustomerIdAsync(customerId, cancellationToken);

            if (survey != null && survey.Count() > 0)
            {
                var surveyDTO = _mapper.Map<List<SurveyDTO>>(survey);
                return surveyDTO;
            }
            throw new NotFoundException($"Survey with ID {customerId} not found");
        }
    }
}
