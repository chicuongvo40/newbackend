﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
namespace CleanArchitecture.Application.Survey.CreateSurvey
{
    public class CreateSurveyCommand : IRequest<Domain.Entities.Survey>, ICommand
    {
        public string SatisfactionRating { get; set; }

        public string Comment { get; set; }

        public DateTime SurveyDate { get; set; }

        public int CustomerId { get; set; }
    }
}
