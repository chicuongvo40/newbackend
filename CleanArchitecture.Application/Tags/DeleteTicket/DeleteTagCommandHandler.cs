﻿using CleanArchitecture.Application.Customers.DeleteCustomer;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tags.DeleteTicket
{
    public class DeleteTagCommandHandler : IRequestHandler<DeleteTagCommand, Unit>
    {
        private readonly ITicketTagRepositories _ticketRepo;
        public DeleteTagCommandHandler(ITicketTagRepositories ticketRepo)
        {
            _ticketRepo = ticketRepo;
        }
        public async Task<Unit> Handle(DeleteTagCommand request, CancellationToken cancellationToken)
        {
            //await _ticketRepo.DeleteTicketSP(request.Id);
            var ticket = await _ticketRepo.FindByIdAsync(request.Id, cancellationToken);

            if (ticket == null)
            {
                throw new NotFoundException("Ticket not found.");
            }
                _ticketRepo.Remove(ticket);
                await _ticketRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            
            return Unit.Value;
        }
    }
}
