﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Tags.DeleteTicket
{
    public class DeleteTagCommand : IRequest<Unit>, ICommand
    {

        public DeleteTagCommand(int _ticketId)
        {
            Id = _ticketId;
        }

        public int Id { get; set; }
    }
}
