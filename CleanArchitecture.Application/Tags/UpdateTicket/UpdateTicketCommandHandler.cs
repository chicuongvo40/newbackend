﻿using CleanArchitecture.Application.Customers.UpdateCustomer;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tags.UpdateTicket
{
    public class UpdateTagCommandHandler : IRequestHandler<UpdateTagCommand, Unit>
    {
        private readonly ITicketTagRepositories _ticketRepo;
        public UpdateTagCommandHandler(ITicketTagRepositories ticketRepo)
        {
            _ticketRepo = ticketRepo;
        }
        public async Task<Unit> Handle(UpdateTagCommand request, CancellationToken cancellationToken)
        {
            var ticket = await _ticketRepo.FindByIdAsync(request.Id, cancellationToken);

            if (ticket == null)
            {
                throw new NotFoundException("Ticket not found.");
            }
            ticket.Id = request.Id;
            ticket.TagName = request.name;
           
            //await _ticketRepo.UpdateTicketSP(ticket);
            _ticketRepo.Update(ticket);
            await _ticketRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}