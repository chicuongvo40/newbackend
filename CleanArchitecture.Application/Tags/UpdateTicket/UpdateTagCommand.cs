﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using MediatR;

namespace CleanArchitecture.Application.Tags.UpdateTicket
{
    public class UpdateTagCommand : IRequest<Unit>, ICommand
    {
        public int Id { get; set; }
        public string name { get; set; }

    }
}
