﻿using CleanArchitecture.Application.Tags.CreateTicket;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tags.CreateTicket
{
    public class CreateTagCommandHandler : IRequestHandler<CreateTagCommand, TicketTag>
    {
        private readonly ITicketTagRepositories _ticketRepo;
        public CreateTagCommandHandler(ITicketTagRepositories ticketRepo)
        {
            _ticketRepo = ticketRepo;
        }
        public async Task<TicketTag> Handle(CreateTagCommand request, CancellationToken cancellationToken)
        {

            var ticket = new TicketTag( 
               );
            ticket.TagName = request.name;
     
            //await _ticketRepo.CreateTicketSP(ticket);
            _ticketRepo.Add(ticket);
            await _ticketRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return ticket;
        }
    }
}
