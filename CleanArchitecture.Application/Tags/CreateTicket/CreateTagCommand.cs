﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;

namespace CleanArchitecture.Application.Tags.CreateTicket
{
    public class CreateTagCommand : IRequest<TicketTag>, ICommand
    {
        public string name { get; set; }

       
    }
}
