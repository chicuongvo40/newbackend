﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;

namespace CleanArchitecture.Application.Tags.GetTicketById
{
    public class GetTagByIdCommand : IRequest<TicketTagDTO>, ICommand
    {
        public int TicketId { get; set; }
    }
}
