﻿using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.OdsService.UpdateOds
{
    public class UpdateOdsCommandHandler : IRequestHandler<UpdateOdsCommand, Unit>
    {
        private readonly IOdsRepositories _odsRepository;
        public UpdateOdsCommandHandler(IOdsRepositories odsRepository)
        {
            _odsRepository = odsRepository;
        }
        public async Task<Unit> Handle(UpdateOdsCommand request, CancellationToken cancellationToken)
        {
            //var ods = new Od();
            //ods.Id = request.Id;
            //ods.OdsBranch = request.OdsBranch;
            //ods.OdsPassword = request.OdsPassword;
            //_odsRepository.Update(ods);
            //await _odsRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            //return Unit.Value;
            var ods = await _odsRepository.FindByIdAsync(request.Id, cancellationToken);

            // Kiểm tra xem Ods có tồn tại trong cơ sở dữ liệu không
            if (ods == null)
            {
                throw new NotFoundException("Ods not found.");
            }

            // Cập nhật thông tin Ods
            ods.OdsBranch = request.OdsBranch;
            ods.OdsPassword = request.OdsPassword;

            // Lưu thay đổi vào cơ sở dữ liệu
            _odsRepository.Update(ods);
            await _odsRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
