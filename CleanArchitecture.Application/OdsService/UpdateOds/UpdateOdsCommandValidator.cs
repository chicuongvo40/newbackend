﻿using CleanArchitecture.Application.OdsService.CreateOds;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.OdsService.UpdateOds
{
    public class UpdateOdsCommandValidator : AbstractValidator<UpdateOdsCommand>
    {
        public UpdateOdsCommandValidator()
        {
            RuleFor(command => command.OdsBranch)
                .NotEmpty().WithMessage("Ods branch is required");

            RuleFor(command => command.OdsPassword)
                .NotEmpty().WithMessage("Ods password is required");
        }
    }
}
