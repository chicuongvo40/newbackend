﻿using AutoMapper;
using CleanArchitecture.Application.Survey.GetSurveyById;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.OdsService.GetOdsById
{
    public class GetOdsByIdCommandHandler : IRequestHandler<GetOdsByIdCommand, OdsDTO>
    {
        private readonly IOdsRepositories _odsRepository;
        private readonly IMapper _mapper;
        public GetOdsByIdCommandHandler(IOdsRepositories odsRepository, IMapper mapper)
        {
            _odsRepository = odsRepository;
            _mapper = mapper;
        }
        public async Task<OdsDTO> Handle(GetOdsByIdCommand request, CancellationToken cancellationToken)
        {
            var odsId = request.OdsId;

            var ods = await _odsRepository.FindByIdAsync(odsId, cancellationToken);

            if (ods != null)
            {
                var odsDTO = _mapper.Map<OdsDTO>(ods);
                return odsDTO;
            }
            throw new NotFoundException($"Ods with ID {odsId} not found");
        }
    }
}
