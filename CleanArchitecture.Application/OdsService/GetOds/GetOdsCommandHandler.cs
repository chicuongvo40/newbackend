﻿using AutoMapper;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.OdsService.GetOds
{
    public class GetOdsCommandHandler : IRequestHandler<GetOdsCommand, PagedResults<OdsDTO>>
    {
        private readonly IOdsRepositories _odsRepository;
        private readonly IMapper _mapper;


        public GetOdsCommandHandler(IOdsRepositories odsRepository, IMapper mapper)
        {
            _odsRepository = odsRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<OdsDTO>> Handle(GetOdsCommand request, CancellationToken cancellationToken)
        {
            var list = await _odsRepository.FindAllAsync();
            var odsDTOs = _mapper.Map<List<OdsDTO>>(list);
            var op = PageHelper<OdsDTO>.Paging(odsDTOs, request.page, request.size);
            return op;
        }
    }
}
