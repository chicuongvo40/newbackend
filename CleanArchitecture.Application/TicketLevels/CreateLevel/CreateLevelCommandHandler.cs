﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Levels.CreateLevel
{
    public class CreateLevelCommandHandler : IRequestHandler<CreateLevelCommand, TicketLevel>
    {
        private readonly ITicketLevelRepositories _levelRepo;
        public CreateLevelCommandHandler(ITicketLevelRepositories levelRepo)
        {
            _levelRepo = levelRepo;
        }
        public async Task<TicketLevel> Handle(CreateLevelCommand request, CancellationToken cancellationToken)
        {
            var level = new TicketLevel(
               
               request.LevelName
                );
            _levelRepo.Add(level);
            await _levelRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return level;
        }
    }
}
