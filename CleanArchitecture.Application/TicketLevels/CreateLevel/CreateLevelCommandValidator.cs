﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Levels.CreateLevel
{
    public class CreateLevelCommandValidator : AbstractValidator<CreateLevelCommand>
    {
        public CreateLevelCommandValidator()
        {
            RuleFor(command => command.LevelName)
             .NotEmpty().WithMessage("Level name is required");
        }
    }
}