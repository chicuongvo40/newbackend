﻿using AutoMapper;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Levels.GetLevel
{
    public class GetLevelCommandHandler : IRequestHandler<GetLevelCommand, PagedResults<TicketLevelDTO>>
    {
        private readonly ITicketLevelRepositories _levelRepo;
        private readonly IMapper _mapper;
        public GetLevelCommandHandler(ITicketLevelRepositories levelRepo, IMapper mapper)
        {
            _levelRepo = levelRepo;
            _mapper = mapper;
        }
        public async Task<PagedResults<TicketLevelDTO>> Handle(GetLevelCommand request, CancellationToken cancellationToken)
        {
            var level = await _levelRepo.FindAllAsync();
            var levelDTO = _mapper.Map<List<TicketLevelDTO>>(level);
            var op = PageHelper<TicketLevelDTO>.Paging(levelDTO, request.page, request.size);
            return op;
        }
    }
}
