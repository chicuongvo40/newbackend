﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Levels.GetLevelById
{
    public class GetLevelByIdCommandHandler : IRequestHandler<GetLevelByIdCommand, TicketLevelDTO>
    {
        private readonly ITicketLevelRepositories _levelRepo;
        private readonly IMapper _mapper;
        public GetLevelByIdCommandHandler(ITicketLevelRepositories levelRepo, IMapper mapper)
        {
            _levelRepo = levelRepo;
            _mapper = mapper;
        }

        public async Task<TicketLevelDTO> Handle(GetLevelByIdCommand request, CancellationToken cancellationToken)
        {
            var levelId = request.LevelId;
            var level = await _levelRepo.FindByIdAsync(levelId, cancellationToken);

            if (level != null)
            {
                var levelDTO = _mapper.Map<TicketLevelDTO>(level);
                return levelDTO;
            }
            throw new NotFoundException($"Level with ID {levelId} not found");
        }
    }
}