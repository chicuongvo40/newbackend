﻿using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Levels.UpdateLevel
{
    public class UpdateLevelCommandHandler : IRequestHandler<UpdateLevelCommand, Unit>
    {
        private readonly ITicketLevelRepositories _levelRepo;


        public UpdateLevelCommandHandler(ITicketLevelRepositories levelRepo)
        {
            _levelRepo = levelRepo;
        }
        public async Task<Unit> Handle(UpdateLevelCommand request, CancellationToken cancellationToken)
        {
            var level = await _levelRepo.FindByIdAsync(request.Id, cancellationToken);

            if (level == null)
            {
                throw new NotFoundException("TicketLevel not found.");
            }
            level.LevelName = request.LevelName;
            //await _levelRepo.UpdateLevelSP(level);
            _levelRepo.Update(level);
            await _levelRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
