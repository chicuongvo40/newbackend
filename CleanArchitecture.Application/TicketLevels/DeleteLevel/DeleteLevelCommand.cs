﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Levels.DeleteLevel
{
    public class DeleteLevelCommand : IRequest<Unit>, ICommand
    {

        public DeleteLevelCommand(int _levelId)
        {
            Id = _levelId;
        }

        public int Id { get; set; }
    }
}
