﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.CreateSource
{
    public class CreateSourceCommand : IRequest<Source>, ICommand
    {
        public string SourceName { get; set; }

    }
}
