﻿using CleanArchitecture.Application.OdsService.CreateOds;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.CreateSource
{
    public class CreateSourceCommandValidator : AbstractValidator<CreateSourceCommand>
    {
        public CreateSourceCommandValidator()
        {
            RuleFor(command => command.SourceName)
                .NotEmpty().WithMessage("SourceName is required");
        }
    }
}
