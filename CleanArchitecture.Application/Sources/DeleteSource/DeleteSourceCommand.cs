﻿using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.DeleteSource
{
    public class DeleteSourceCommand : IRequest<Unit>, ICommand
    {
        public DeleteSourceCommand(int sourceId)
        {
            Id = sourceId;
        }
        public int Id { get; set; }
    }
}
