﻿using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.UpdateSource
{
    public class UpdateSourceCommand : IRequest<Unit>, ICommand
    {
        public int Id { get; set; }
        public string SourceName { get; set; }
    }
}
