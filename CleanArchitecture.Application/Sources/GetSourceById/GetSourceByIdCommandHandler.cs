﻿using AutoMapper;
using CleanArchitecture.Application.Branchs.GetBranchById;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.GetSourceById
{
    public class GetSourceByIdCommandHandler : IRequestHandler<GetSourceByIdCommand, SourceDTO>
    {
        private readonly ISourceRepositories _sourceRepo;
        private readonly IMapper _mapper;
        public GetSourceByIdCommandHandler(ISourceRepositories sourceRepo, IMapper mapper)
        {
            _sourceRepo = sourceRepo;
            _mapper = mapper;
        }
        public async Task<SourceDTO> Handle(GetSourceByIdCommand request, CancellationToken cancellationToken)
        {
            var sourceId = request.SourceId;

            var source = await _sourceRepo.FindByIdAsync(sourceId, cancellationToken);

            if (source != null)
            {
                var sourceDTO = _mapper.Map<SourceDTO>(source);
                return sourceDTO;
            }
            throw new NotFoundException($"Branch with ID {sourceId} not found");
        }
    }
}

