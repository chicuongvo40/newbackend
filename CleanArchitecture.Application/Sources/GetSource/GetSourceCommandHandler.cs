﻿using AutoMapper;
using CleanArchitecture.Application.TicketTypes.GetTicketType;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.GetSource
{
    public class GetSourceCommandHandler : IRequestHandler<GetSourceCommand, PagedResults<SourceDTO>>
    {
        private readonly ISourceRepositories _sourceRepository;
        private readonly IMapper _mapper;

        public GetSourceCommandHandler(ISourceRepositories sourceRepository, IMapper mapper)
        {
            _sourceRepository = sourceRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<SourceDTO>> Handle(GetSourceCommand request, CancellationToken cancellationToken)
        {
            var source = await _sourceRepository.FindAllAsync();
            var sourceDTOs = _mapper.Map<List<SourceDTO>>(source);
            var op = PageHelper<SourceDTO>.Paging(sourceDTOs, request.page, request.size);
            return op;
        }
    }
}
