﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Contracts.GetContractByPhone
{
    public class GetContractByPhoneCommand : IRequest<List<ContractDTO>>, ICommand
    {
        public string Phone { get; set; }
    }
}
