﻿using AutoMapper;
using CleanArchitecture.Application.Contracts.GetContractByUserId;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Contracts.GetContractByPhone
{
    public class GetContractByPhoneCommandHandler : IRequestHandler<GetContractByPhoneCommand, List<ContractDTO>>
    {
        private readonly IContractRepositories _userRepository;
        private readonly IMapper _mapper;

        public GetContractByPhoneCommandHandler(IContractRepositories userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<List<ContractDTO>> Handle(GetContractByPhoneCommand request, CancellationToken cancellationToken)
        {
            var phone = request.Phone;
            var users = await _userRepository.FindByPhoneAsync(phone, cancellationToken);

            if (users != null && users.Count() > 0)
            {
                var userDTOs = _mapper.Map<List<ContractDTO>>(users);
                return userDTOs;
            }

            throw new NotFoundException($"No users found with {phone}");
        }
    }
}
