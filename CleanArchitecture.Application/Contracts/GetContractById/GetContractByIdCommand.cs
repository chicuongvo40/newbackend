﻿using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
namespace CleanArchitecture.Application.Contracts.GetContractById
{
    public class GetContractByIdCommand : IRequest<ContractDTO>, ICommand
    {
        public int ContractId { get; set; }
    }
}
