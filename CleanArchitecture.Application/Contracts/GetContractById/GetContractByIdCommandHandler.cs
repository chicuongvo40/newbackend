﻿
using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Contracts.GetContractById
{
    public class GetContractByIdCommandHandler : IRequestHandler<GetContractByIdCommand, ContractDTO>
    {
        private readonly IContractRepositories _contractRepo;
        private readonly IMapper _mapper;

        public GetContractByIdCommandHandler(IContractRepositories contractRepo, IMapper mapper)
        {
            _contractRepo = contractRepo;
            _mapper = mapper;
        }

        public async Task<ContractDTO> Handle(GetContractByIdCommand request, CancellationToken cancellationToken)
        {
            var contractId = request.ContractId;

            var contract = await _contractRepo.FindByIdAsync(contractId, cancellationToken);

            if (contract != null)
            {
                var contractDTO = _mapper.Map<ContractDTO>(contract);
                return contractDTO;
            }

            throw new NotFoundException($"Contract with ID {contractId} not found");
        }
    }
}
