﻿using AutoMapper;
using CleanArchitecture.Application.Users.GetUserByRoleId;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Contracts.GetContractByUserId
{
    public class GetContractByUserIdCommandHandler : IRequestHandler<GetContractByUserIdCommand, DetailContract>
    {
        private readonly IContractRepositories _userRepository;
        private readonly ICustomerRepositories _customerRepository;
        private readonly IMapper _mapper;

        public GetContractByUserIdCommandHandler(IContractRepositories userRepository, IMapper mapper, ICustomerRepositories customerRepository)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _customerRepository = customerRepository;
        }

        public async Task<DetailContract> Handle(GetContractByUserIdCommand request, CancellationToken cancellationToken)
        {
            var customerId = request.CustomerId;
            var os = await _customerRepository.GetCustomerById(customerId);
            var users = await _userRepository.FindByCustomerIdAsync(customerId, cancellationToken);
            List<DetailContract> list = new List<DetailContract>();
            if (users != null && users.Count() > 0)
            {
                var userDTOs = _mapper.Map<List<ContractDTO>>(users);
                foreach (var op in users) { 
                    var ops = new DetailContract();
                    ops.Id = op.Id;
                    ops.CustomerId =  os;   
                    ops.ContractType = op.ContractType;
                    ops.TermsAndConditions = op.TermsAndConditions;
                    ops.TimeStart = op.TimeStart;
                    ops.TimeEnd = op.TimeEnd;
                    ops.LastEditedTime = op.LastEditedTime;
                    ops.Code = op.Code;
                    ops.Address = op.Address;
                    ops.Title = op.Title;
                    ops.CallNumber = op.CallNumber;
                    ops.BranchId = op.BranchId;
                    list.Add(ops);
                    
                }
                return list[0];
            }

            throw new NotFoundException($"No users found with {customerId}");
        }
    }
}
