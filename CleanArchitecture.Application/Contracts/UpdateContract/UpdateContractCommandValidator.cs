﻿using CleanArchitecture.Application.Contracts.CreateContract;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Contracts.UpdateContract
{
    public class UpdateContractCommandValidator : AbstractValidator<UpdateContractCommand>
    {
        public UpdateContractCommandValidator()
        {
            RuleFor(command => command.ContractType)
                    .NotEmpty().WithMessage("Contract type is required")
                    .MaximumLength(50).WithMessage("Contract type must not exceed 50 characters.");

        }
    }
}
