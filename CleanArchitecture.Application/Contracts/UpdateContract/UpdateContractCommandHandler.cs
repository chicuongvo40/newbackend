﻿using CleanArchitecture.Application.Branchs.UpdateBrach;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Contracts.UpdateContract
{
    public class UpdateContractCommandHandler : IRequestHandler<UpdateContractCommand, Unit>
    {
        private readonly IContractRepositories _contractRepository;
        public UpdateContractCommandHandler(IContractRepositories contractRepository)
        {
            _contractRepository = contractRepository;
        }
        public async Task<Unit> Handle(UpdateContractCommand request, CancellationToken cancellationToken)
        {
            var contract = await _contractRepository.FindByIdAsync(request.Id, cancellationToken);

            if (contract == null)
            {
                throw new NotFoundException("Contract not found.");
            }
            contract.CustomerId = request.CustomerId;
            contract.ContractType = request.ContractType;
            contract.TermsAndConditions = request.TermsAndConditions;
            contract.TimeStart = request.TimeStart;
            contract.TimeEnd = request.TimeEnd;
            contract.LastEditedTime = request.LastEditedTime;
            contract.Code = request.Code;
            contract.Title = request.Title;
            contract.Address = request.Address;
            contract.CallNumber = request.CallNumber;
            contract.BranchId = request.BranchId;
            // await _contractRepository.UpdateContractSP(bra);
            _contractRepository.Update(contract);
            await _contractRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
