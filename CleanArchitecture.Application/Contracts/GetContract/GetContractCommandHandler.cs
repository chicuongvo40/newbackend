﻿using AutoMapper;
using CleanArchitecture.Application.Branchs.GetBranch;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Contracts.GetContract
{
    public class GetContractCommandHandler : IRequestHandler<GetContractCommand, PagedResults<ContractDTO>>
    {
        private readonly IContractRepositories _contractRepository;
        private readonly IMapper _mapper;

        public GetContractCommandHandler(IContractRepositories contractRepository, IMapper mapper)
        {
            _contractRepository = contractRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<ContractDTO>> Handle(GetContractCommand request, CancellationToken cancellationToken)
        {
            var contract = await _contractRepository.FindAllAsync();
            var contractDTOs = _mapper.Map<List<ContractDTO>>(contract);
            var op = PageHelper<ContractDTO>.Paging(contractDTOs, request.page, request.size);
            return op;
        }
    }
}
