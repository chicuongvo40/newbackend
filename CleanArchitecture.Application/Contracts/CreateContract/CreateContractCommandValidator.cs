﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace CleanArchitecture.Application.Contracts.CreateContract
{
    public class CreateContractCommandValidator : AbstractValidator<CreateContractCommand>
    {
        public CreateContractCommandValidator()
        {
            RuleFor(command => command.ContractType)
                    .NotEmpty().WithMessage("Contract type is required")
                    .MaximumLength(50).WithMessage("Contract type must not exceed 50 characters.");
            RuleFor(command => command.Address)
                   .NotEmpty().WithMessage("Contract type is required");
        }
    }
}