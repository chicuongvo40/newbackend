﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.CreateTicket
{
    public class CreateTicketCommandHandler : IRequestHandler<CreateTicketCommand, Ticket>
    {
        private readonly ITicketRepositories _ticketRepo;
        public CreateTicketCommandHandler(ITicketRepositories ticketRepo)
        {
            _ticketRepo = ticketRepo;
        }
        public async Task<Ticket> Handle(CreateTicketCommand request, CancellationToken cancellationToken)
        {
            List<Log> listlog = new List<Log>();
            var _log = new Log(
               );
            _log.Note = "Ticket đã được tạo";
            _log.CreatedDate = DateTime.Now;
            _log.Code = "";
            _log.ImgUrl = "";
            listlog.Add( _log );
            var ticket = new Ticket(
              request.BranchId,
              request.Note,
              request.CustomerId,
              request.TicketTypeId,
              request.LevelId,
              request.TicketStatusId,
              request.CreatedDate,
              request.CreatedBy,
              request.AssignedUserId,
              request.Code,
              request.Title,
              listlog
               );
            ticket.AssignedUserId = null;
            ticket.Logs = listlog;
            //await _ticketRepo.CreateTicketSP(ticket);
            _ticketRepo.Add(ticket);
            await _ticketRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return ticket;
        }
    }
}
