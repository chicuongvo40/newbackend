﻿using AutoMapper;
using CleanArchitecture.Application.Tickets.GetTicketNameById;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketNameByPhone
{
    public class GetTicketNameByPhonedHandler : IRequestHandler<GetTicketNameByPhoneCommand, List<TicketName>>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        private readonly IBranchRepositories _branch;
        private readonly ICustomerRepositories _customer;
        private readonly ITicketLevelRepositories _level;
        private readonly ITicketTypeRepositories _ticketType;
        private readonly ITicketStatusRepositories _ticketStatus;
        private readonly IUserRepository _userRepository;
        public GetTicketNameByPhonedHandler(ITicketRepositories ticketRepo, IMapper mapper, IBranchRepositories branch, ICustomerRepositories customer, ITicketLevelRepositories level, ITicketTypeRepositories ticketType, ITicketStatusRepositories ticketStatus, IUserRepository userRepository, ITicketTagConnectRepositories ticketTagConnectRepositories, ITicketTagRepositories ticketTag)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
            _branch = branch;
            _customer = customer;
            _level = level;
            _ticketType = ticketType;
            _ticketStatus = ticketStatus;
            _userRepository = userRepository;
          
        }
        public async Task<List<TicketName>> Handle(GetTicketNameByPhoneCommand request, CancellationToken cancellationToken)
        {
            Console.WriteLine("Đã tới service");
            Debug.WriteLine("Đã tới service");
            var ticket = await _customer.FindByPhoneNumberAsync(request.PhoneNumber);
            Debug.WriteLine("Đã tới await và id của user là " + ticket.Id.ToString());
            if (ticket == null)
            {
                return null;
            }
            var tickets = await _ticketRepo.FindByCustomerIdAsync(ticket.Id);
            Debug.WriteLine("Đã xong await");
            List<TicketName> list = new List<TicketName>();
            foreach (var item in tickets)
            {
                var ops = new TicketName();
                ops.Id = item.Id;
                ops.BranchId = item.BranchId;
                ops.Note = item.Note;
                ops.CustomerId = _customer.GetCustomerById(item.CustomerId).Result;
                ops.LevelId = _level.FindByIdAsync(item.LevelId).Result.LevelName;
                ops.TicketTypeId = _ticketType.FindByIdAsync(item.TicketTypeId).Result.TicketTypeName;
                ops.TicketStatusId = _ticketStatus.FindByIdAsync(item.TicketStatusId).Result.StatusName;
                ops.CreatedDate = item.CreatedDate;
                ops.AssignedUserId = item.AssignedUserId ;
                ops.Code = item.Code;
                ops.Title = item.Title;
                list.Add(ops);
            }
            return list;
        }
    }
}
