﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketNameByPhone
{
    public class GetTicketNameByPhoneCommand : IRequest<List<TicketName>>, ICommand
    {
        public string PhoneNumber { get; set; }
    }
}
