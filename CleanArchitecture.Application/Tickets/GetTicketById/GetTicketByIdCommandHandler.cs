﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketById
{
    public class GetTicketByIdCommandHandler : IRequestHandler<GetTicketByIdCommand, TicketDTO>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        public GetTicketByIdCommandHandler(ITicketRepositories ticketRepo, IMapper mapper)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
        }

        public async Task<TicketDTO> Handle(GetTicketByIdCommand request, CancellationToken cancellationToken)
        {
            var ticketId = request.TicketId;
            var ticket = await _ticketRepo.FindByIdAsync(ticketId, cancellationToken);

            if (ticket != null)
            {
                var ticketDTO = _mapper.Map<TicketDTO>(ticket);
                return ticketDTO;
            }
            throw new NotFoundException($"Ticket with ID {ticketId} not found");
        }
    }
}
