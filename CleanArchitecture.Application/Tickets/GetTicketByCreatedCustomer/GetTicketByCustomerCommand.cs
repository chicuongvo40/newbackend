﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketByCreatedCustomer
{
    public class GetTicketByCreatedCustomerCommand : IRequest<List<TicketName>>, ICommand
    {
        
    }
}
