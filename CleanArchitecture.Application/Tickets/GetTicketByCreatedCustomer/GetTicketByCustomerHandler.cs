﻿using AutoMapper;
using CleanArchitecture.Application.Tickets.GetTicketNameById;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketByCreatedCustomer
{
    public class GetTicketByCreatedCustomerHandler : IRequestHandler<GetTicketByCreatedCustomerCommand, List<TicketName>>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        private readonly IBranchRepositories _branch;
        private readonly ICustomerRepositories _customer;
        private readonly ITicketLevelRepositories _level;
        private readonly ITicketTypeRepositories _ticketType;
        private readonly ITicketStatusRepositories _ticketStatus;
        private readonly IUserRepository _userRepository;
        public GetTicketByCreatedCustomerHandler(ITicketRepositories ticketRepo, IMapper mapper, IBranchRepositories branch, ICustomerRepositories customer, ITicketLevelRepositories level, ITicketTypeRepositories ticketType, ITicketStatusRepositories ticketStatus, IUserRepository userRepository, ITicketTagConnectRepositories ticketTagConnectRepositories, ITicketTagRepositories ticketTag)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
            _branch = branch;
            _customer = customer;
            _level = level;
            _ticketType = ticketType;
            _ticketStatus = ticketStatus;
            _userRepository = userRepository;
          
        }
        public async Task<List<TicketName>> Handle(GetTicketByCreatedCustomerCommand request, CancellationToken cancellationToken)
        {
            var ticket = await _ticketRepo.FindAllAsync();
            
            if (ticket == null)
            {
                return null;
            }
            List<TicketName> list = new List<TicketName>();
            foreach (var item in ticket)
            {   if (item.CreatedBy == "Customer")
                {
                    var ops = new TicketName();
                    ops.Id = item.Id;
                    ops.BranchId = item.BranchId;
                    ops.Note = item.Note;
                    ops.CustomerId = _customer.GetCustomerById(item.CustomerId).Result;
                    ops.LevelId = _level.FindByIdAsync(item.LevelId).Result.LevelName;
                    ops.TicketTypeId = _ticketType.FindByIdAsync(item.TicketTypeId).Result.TicketTypeName;
                    ops.TicketStatusId = _ticketStatus.FindByIdAsync(item.TicketStatusId).Result.StatusName;
                    ops.CreatedDate = item.CreatedDate;
                    ops.AssignedUserId = item.AssignedUserId;
                    ops.Code = item.Code;
                    ops.Title = item.Title;
                    ops.CreatedBy = item.CreatedBy;
                    list.Add(ops);
                }
            }
            return list;
        }
    }
}
