﻿using AutoMapper;

using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;


namespace CleanArchitecture.Application.Tickets.GetTicketNameById
{
    public class GetTicketNameByIdCommandHandler : IRequestHandler<GetTicketNameByIdCommand, TicketName>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        private readonly IBranchRepositories _branch;
        private readonly ICustomerRepositories _customer;
        private readonly ITicketLevelRepositories _level;
        private readonly ITicketTypeRepositories _ticketType;
        private readonly ITicketStatusRepositories _ticketStatus;
        private readonly IUserRepository _userRepository;
        private readonly ITicketTagConnectRepositories _ticketTagConnectRepositories;
        private readonly ITicketTagRepositories _ticketTag;
        public GetTicketNameByIdCommandHandler(ITicketRepositories ticketRepo, IMapper mapper, IBranchRepositories branchRepositories, ICustomerRepositories customer, ITicketLevelRepositories level, ITicketTypeRepositories ticketType, ITicketStatusRepositories ticketStatus, IUserRepository userRepository, ITicketTagConnectRepositories ticketTagConnectRepositories, ITicketTagRepositories ticketTag)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
            _branch = branchRepositories;
            _customer = customer;
            _level = level;
            _ticketType = ticketType;
            _ticketStatus = ticketStatus;
            _userRepository = userRepository;
            _ticketTagConnectRepositories = ticketTagConnectRepositories;
            _ticketTag = ticketTag;
        }
        public async Task<TicketName> Handle(GetTicketNameByIdCommand request, CancellationToken cancellationToken)
        {
            var item = await _ticketRepo.FindByIdAsync(request.TicketId);
            var ops = new TicketName();
            ops.Id = item.Id;
            ops.BranchId = item.BranchId;
            ops.Note = item.Note;
            ops.CustomerId = _customer.GetCustomerById(item.CustomerId).Result;
            ops.LevelId = _level.FindByIdAsync(item.LevelId).Result.LevelName;
            ops.TicketTypeId = _ticketType.FindByIdAsync(item.TicketTypeId).Result.TicketTypeName;
            ops.TicketStatusId = _ticketStatus.FindByIdAsync(item.TicketStatusId).Result.StatusName;
            ops.CreatedDate = item.CreatedDate;
            ops.AssignedUserId = item.AssignedUserId ;
            ops.Code = item.Code;
            ops.Title = item.Title;
            // Fetch tags for this ticket
            var ad = await _ticketTagConnectRepositories.FindByTicketId(item.Id);
            List<string> listag = new List<string>();
            foreach (var tagConnection in ad)
            {
                var tag = await _ticketTag.FindByIdAsync(tagConnection.TicketTagId);
                listag.Add(tag.TagName);
            }
            ops.Tags = listag;

            return ops;
        }
        
    }
}