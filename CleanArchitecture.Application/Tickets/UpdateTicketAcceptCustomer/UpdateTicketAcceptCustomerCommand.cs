﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using MediatR;

namespace CleanArchitecture.Application.Tickets.UpdateTicketAcceptCustomer
{
    public class UpdateTicketAcceptCustomerCommand : IRequest<Unit>, ICommand
    {
        public int Id { get; set; }
        public int BranchId { get; set; }

        public string Note { get; set; }

        public int CustomerId { get; set; }

        public int TicketTypeId { get; set; }

        public int LevelId { get; set; }

        public int TicketStatusId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public int AssignedUserId { get; set; }

        public string Code { get; set; }
        public string Title { get; set; }
    }
}
