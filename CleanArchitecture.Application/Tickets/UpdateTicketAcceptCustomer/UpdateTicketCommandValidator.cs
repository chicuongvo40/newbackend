﻿using CleanArchitecture.Application.Tickets.CreateTicket;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.UpdateTicketAcceptCustomer
{
    internal class UpdateTicketAcceptCustomerCommandValidator : AbstractValidator<UpdateTicketAcceptCustomerCommand>
    {
        public UpdateTicketAcceptCustomerCommandValidator()
        {
                     RuleFor(command => command.Note)
                .MaximumLength(500).WithMessage("Note must not exceed 500 characters.");
        }
    }
}