﻿using CleanArchitecture.Application.Customers.UpdateCustomer;
using CleanArchitecture.Application.Tickets.UpdateTicketAcceptCustomer;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.UpdateTicketAcceptCustomer
{
    public class UpdateTicketAcceptCustomerCommandHandler : IRequestHandler<UpdateTicketAcceptCustomerCommand, Unit>
    {
        private readonly ITicketRepositories _ticketRepo;
        public UpdateTicketAcceptCustomerCommandHandler(ITicketRepositories ticketRepo)
        {
            _ticketRepo = ticketRepo;
        }
        public async Task<Unit> Handle(UpdateTicketAcceptCustomerCommand request, CancellationToken cancellationToken)
        {
            var ticket = await _ticketRepo.FindByIdAsync(request.Id, cancellationToken);

            if (ticket == null)
            {
                throw new NotFoundException("Ticket not found.");
            }
            ticket.BranchId = request.BranchId;
            ticket.Note = request.Note;
            ticket.CustomerId = request.CustomerId;
            ticket.TicketTypeId = request.TicketTypeId;
            ticket.LevelId = request.LevelId;
            ticket.TicketStatusId = request.TicketStatusId;
            ticket.AssignedUserId = null;
            ticket.CreatedDate = request.CreatedDate;
            ticket.CreatedBy = request.CreatedBy;
            ticket.Title = request.Title;
            //await _ticketRepo.UpdateTicketSP(ticket);
            _ticketRepo.Update(ticket);
            await _ticketRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}