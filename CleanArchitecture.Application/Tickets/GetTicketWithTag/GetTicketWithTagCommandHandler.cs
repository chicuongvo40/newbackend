﻿using AutoMapper;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketWithTag
{
    public class GetTicketWithTagCommandHandler : IRequestHandler<GetTicketWithTagCommand, TicketWithTagDTO>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        private readonly IBranchRepositories _branch;
        private readonly ICustomerRepositories _customer;
        private readonly ITicketLevelRepositories _level;
        private readonly ITicketTypeRepositories _ticketType;
        private readonly ITicketStatusRepositories _ticketStatus;
        private readonly IUserRepository _userRepository;
        private readonly ITicketTagConnectRepositories _ticketTagConnectRepositories;
        private readonly ITicketTagRepositories _ticketTag;

        public GetTicketWithTagCommandHandler(ITicketRepositories ticketRepo, IMapper mapper, IBranchRepositories branch, ICustomerRepositories customer, ITicketLevelRepositories level, ITicketTypeRepositories ticketType, ITicketStatusRepositories ticketStatus, IUserRepository userRepository, ITicketTagConnectRepositories ticketTagConnectRepositories, ITicketTagRepositories ticketTag)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
            _branch = branch;
            _customer = customer;
            _level = level;
            _ticketType = ticketType;
            _ticketStatus = ticketStatus;
            _userRepository = userRepository;
            _ticketTagConnectRepositories = ticketTagConnectRepositories;
            _ticketTag = ticketTag;
        }
        public async Task<TicketWithTagDTO> Handle(GetTicketWithTagCommand request, CancellationToken cancellationToken)
        {

            var ticket = await _ticketRepo.FindByBranch(request.id);
            if (ticket == null)
            {
                return null;
            }
            List<TicketName> list1 = new List<TicketName>();
            List<TicketName> list2 = new List<TicketName>();
            List<TicketName> list3 = new List<TicketName>();
            List<TicketName> list4 = new List<TicketName>();

            foreach (var item in ticket)
            {
                var ops = new TicketName();
                ops.Id = item.Id;
                ops.BranchId = item.BranchId;
                ops.Note = item.Note;
                ops.CustomerId = _customer.GetCustomerById(item.CustomerId).Result;
                ops.LevelId = _level.FindByIdAsync(item.LevelId).Result.LevelName;
                ops.TicketTypeId = _ticketType.FindByIdAsync(item.TicketTypeId).Result.TicketTypeName;
                ops.TicketStatusId = _ticketStatus.FindByIdAsync(item.TicketStatusId).Result.StatusName;
                ops.CreatedDate = item.CreatedDate;
                ops.AssignedUserId = item.AssignedUserId ;
                ops.Code = item.Code;
                ops.Title = item.Title;
                if (item.TicketStatusId == 1 && item.CreatedBy != "Customer")
                {
                    list1.Add(ops);
                }
                if (item.TicketStatusId == 2)
                {
                    list2.Add(ops);
                }
                if (item.TicketStatusId == 3)
                {
                    list3.Add(ops);
                }
                if (item.TicketStatusId == 4)
                {
                    list4.Add(ops);
                }

            }
            foreach (var item in list1)
            {
                var op = await _ticketTagConnectRepositories.FindByTicketId(item.Id);
                List<string> listag = new List<string>();
                foreach (var item2 in op)
                {
                    var ad = await _ticketTag.FindByIdAsync(item2.TicketTagId);
                    listag.Add(ad.TagName);
                }
                item.Tags = listag;
            }
            foreach (var item in list2)
            {
                var op = await _ticketTagConnectRepositories.FindByTicketId(item.Id);
                List<string> listag = new List<string>();
                foreach (var item2 in op)
                {
                    var ad = await _ticketTag.FindByIdAsync(item2.TicketTagId);
                    listag.Add(ad.TagName);
                }
                item.Tags = listag;
            }
            foreach (var item in list3)
            {
                var op = await _ticketTagConnectRepositories.FindByTicketId(item.Id);
                List<string> listag = new List<string>();
                foreach (var item2 in op)
                {
                    var ad = await _ticketTag.FindByIdAsync(item2.TicketTagId);
                    listag.Add(ad.TagName);
                }
                item.Tags = listag;
            }
            foreach (var item in list4)
            {
                var op = await _ticketTagConnectRepositories.FindByTicketId(item.Id);
                List<string> listag = new List<string>();
                foreach (var item2 in op)
                {
                    var ad = await _ticketTag.FindByIdAsync(item2.TicketTagId);
                    listag.Add(ad.TagName);
                }
                item.Tags = listag;
            }
            TicketWithTagDTO ticketWithTagDTO = new TicketWithTagDTO();
            list1.Reverse();
            list2.Reverse();
            list3.Reverse();
            list4.Reverse();
            ticketWithTagDTO.StatusOne = list1;    
            ticketWithTagDTO.StatusTwo = list2;    
            ticketWithTagDTO.StatusThree = list3;    
            ticketWithTagDTO.StatusFour = list4;    
            return ticketWithTagDTO;
        }
    }
}
