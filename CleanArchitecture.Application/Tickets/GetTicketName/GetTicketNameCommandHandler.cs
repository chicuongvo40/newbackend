﻿using AutoMapper;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;


namespace CleanArchitecture.Application.Tickets.GetTicketName
{
    public class GetTicketNameCommandHandler : IRequestHandler<GetTicketNameCommand, PagedResults<TicketName>>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        private readonly IBranchRepositories _branch;
        private readonly ICustomerRepositories _customer;
        private readonly ITicketLevelRepositories _level;
        private readonly ITicketTypeRepositories _ticketType;
        private readonly ITicketStatusRepositories _ticketStatus;
        private readonly IUserRepository _userRepository;
        private readonly ITicketTagConnectRepositories _ticketTagConnectRepositories;
        private readonly ITicketTagRepositories _ticketTag;
        public GetTicketNameCommandHandler(ITicketRepositories ticketRepo, IMapper mapper, IBranchRepositories branch, ICustomerRepositories customer, ITicketLevelRepositories level, ITicketTypeRepositories ticketType, ITicketStatusRepositories ticketStatus, IUserRepository userRepository, ITicketTagConnectRepositories ticketTagConnectRepositories, ITicketTagRepositories ticketTag)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
            _branch = branch;
            _customer = customer;
            _level = level;
            _ticketType = ticketType;
            _ticketStatus = ticketStatus;
            _userRepository = userRepository;
            _ticketTagConnectRepositories = ticketTagConnectRepositories;
            _ticketTag = ticketTag;
        }
        public async Task<PagedResults<TicketName>> Handle(GetTicketNameCommand request, CancellationToken cancellationToken)
        {
            var ticket = await _ticketRepo.FindAllAsync();
            // var ticketDTO = _mapper.Map<List<TicketName>>(ticket);
            List<TicketName> list = new List<TicketName>();
            foreach (var item in ticket)
            {
                var ops = new TicketName();
                ops.Id = item.Id;
                ops.BranchId = item.BranchId;
                ops.Note = item.Note;
                ops.CustomerId = _customer.GetCustomerById(item.CustomerId).Result;
                ops.LevelId = _level.FindByIdAsync(item.LevelId).Result.LevelName;
                ops.TicketTypeId = _ticketType.FindByIdAsync(item.TicketTypeId).Result.TicketTypeName;
                ops.TicketStatusId = _ticketStatus.FindByIdAsync(item.TicketStatusId).Result.StatusName;
                ops.CreatedDate = item.CreatedDate;
                ops.AssignedUserId = item.AssignedUserId ;
                ops.Code = item.Code;
                ops.Title = item.Title;
                list.Add(ops);
            }
            foreach (var item in list)
            {
                var ad = await _ticketTagConnectRepositories.FindByTicketId(item.Id);
                List<string> listag = new List<string>();
                foreach (var item2 in ad)
                {
                    var ads = await _ticketTag.FindByIdAsync(item2.TicketTagId);
                    listag.Add(ads.TagName);
                }
                item.Tags = listag;
            }
            var op = PageHelper<TicketName>.Paging(list, request.page, request.size);
            return op;
        }
    }
}
