﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.CreateTicket
{
    public class CreateTicketByCustomerCommandHandler : IRequestHandler<CreateTicketByCustomerCommand, Ticket>
    {
        private readonly ITicketRepositories _ticketRepo;
        public CreateTicketByCustomerCommandHandler(ITicketRepositories ticketRepo)
        {
            _ticketRepo = ticketRepo;
        }
        public async Task<Ticket> Handle(CreateTicketByCustomerCommand request, CancellationToken cancellationToken)
        {
            List<Log> listlog = new List<Log>();
            var _log = new Log(
               );
            _log.Note = "Ticket đã được tạo bởi khách hàng";
            _log.CreatedDate = DateTime.Now;
            _log.Code = "";
            _log.ImgUrl = "";
            listlog.Add( _log );
            var ticket = new Ticket(
               );
   
            ticket.BranchId = request.BranchId;
            ticket.Note = request.Note;
            ticket.CustomerId = request.CustomerId;
            ticket.TicketTypeId = request.TicketTypeId;
            ticket.LevelId = request.LevelId;
            ticket.TicketStatusId = request.TicketStatusId;
            ticket.CreatedDate = request.CreatedDate;
            ticket.Title = request.Title;
            ticket.AssignedUserId = null;
            ticket.CreatedBy = "Customer";
            ticket.Code = "CSTk2024123";
            ticket.Logs = listlog;
            Console.WriteLine( ticket.ToString);
            //await _ticketRepo.CreateTicketSP(ticket);
            _ticketRepo.Add(ticket);
            try
            {
                await _ticketRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex) { throw ex; }
            return ticket;
        }
    }
}
