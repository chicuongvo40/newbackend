﻿using CleanArchitecture.Application.Customers.CreateCustomer;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Branchs.CreateBranch
{
    public class CreateBranchCommandHandler : IRequestHandler<CreateBranchCommand, Branch>
    {
        private readonly IBranchRepositories _braRepository;
        public CreateBranchCommandHandler(IBranchRepositories braRepository)
        {
            _braRepository = braRepository;
        }
        public async Task<Branch> Handle(CreateBranchCommand request, CancellationToken cancellationToken)
        {
            var _branch = new Branch(
              request.OdsId,
               request.Address,
               request.BranchName,
               request.Email,
               request.CallNumber
                );
            //await _braRepository.CreateBranchSP(_branch);
            _braRepository.Add(_branch);
            await _braRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return _branch;
        }
    }
}
