﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Branchs.CreateBranch
{
    public class CreateBranchCommand : IRequest<Branch>, ICommand
    {
        public int OdsId { get; set; }

        public string Address { get; set; }

        public string BranchName { get; set; }
        public string Email { get; set; }

        public string CallNumber { get; set; }
    }
}
