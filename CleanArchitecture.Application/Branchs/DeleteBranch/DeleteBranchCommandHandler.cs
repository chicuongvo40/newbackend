﻿using CleanArchitecture.Application.Customers.DeleteCustomer;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Branchs.DeleteBranch
{
    public class DeleteBranchCommandHandler : IRequestHandler<DeleteBranchCommand, Unit>
    {
        private readonly IBranchRepositories _braRepository;

        public DeleteBranchCommandHandler(IBranchRepositories braRepository)
        {
            _braRepository = braRepository;
        }
        public async Task<Unit> Handle(DeleteBranchCommand request, CancellationToken cancellationToken)
        {
            var branch = await _braRepository.FindByIdAsync(request.Id, cancellationToken);

            if (branch == null)
            {
                throw new NotFoundException("Branch not found.");
            }

            _braRepository.Remove(branch);
            await _braRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
