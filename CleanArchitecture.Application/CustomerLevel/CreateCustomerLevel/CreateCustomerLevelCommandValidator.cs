﻿using CleanArchitecture.Application.Customers.CreateCustomer;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CustomerLevel.CreateCustomerLevel
{
    public class CreateCustomerLevelCommandValidator : AbstractValidator<CreateCustomerLevelCommand>
    {
        public CreateCustomerLevelCommandValidator()
        {
            RuleFor(command => command.LevelName)
            .NotEmpty().WithMessage("LevelName is required");
        }
    }
}
