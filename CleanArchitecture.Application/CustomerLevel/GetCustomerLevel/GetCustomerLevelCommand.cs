﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CustomerLevel.GetCustomerLevel
{
    public class GetCustomerLevelCommand : IRequest<PagedResults<CustomerLevelDTO>>, ICommand
    {
        public int page { get; set; }

        public int size { get; set; }
    }
}

