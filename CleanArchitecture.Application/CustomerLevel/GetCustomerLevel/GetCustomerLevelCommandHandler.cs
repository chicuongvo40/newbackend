﻿using AutoMapper;
using CleanArchitecture.Application.Sources.GetSource;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CustomerLevel.GetCustomerLevel
{
    public class GetCustomerLevelCommandHandler : IRequestHandler<GetCustomerLevelCommand, PagedResults<CustomerLevelDTO>>
    {
        private readonly ICustomerLevelRepositories _customerlevelRepository;
        private readonly IMapper _mapper;

        public GetCustomerLevelCommandHandler(ICustomerLevelRepositories sourceRepository, IMapper mapper)
        {
            _customerlevelRepository = sourceRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<CustomerLevelDTO>> Handle(GetCustomerLevelCommand request, CancellationToken cancellationToken)
        {
            var customerlevel = await _customerlevelRepository.FindAllAsync();
            var customerlevelDTOs = _mapper.Map<List<CustomerLevelDTO>>(customerlevel);
            var op = PageHelper<CustomerLevelDTO>.Paging(customerlevelDTOs, request.page, request.size);
            return op;
        }
    }
}
