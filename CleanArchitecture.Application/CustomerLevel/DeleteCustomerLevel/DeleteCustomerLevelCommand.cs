﻿using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CustomerLevel.DeleteCustomerLevel
{
    public class DeleteCustomerLevelCommand : IRequest<Unit>, ICommand
    {
        public DeleteCustomerLevelCommand(int customerLevelId)
        {
            Id = customerLevelId;
        }
        public int Id { get; set; }
    }
}