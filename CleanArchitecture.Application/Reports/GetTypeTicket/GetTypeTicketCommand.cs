﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Reports.GetTypeTicket
{
    public class GetTypeTicketCommand : IRequest<object>, ICommand
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }
}
