﻿using AutoMapper;
using CleanArchitecture.Application.CallHistorys.GetCallHistoryByDay;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Reports.GetTypeTicket
{
    public class GetTypeTicketCommandHandler : IRequestHandler<GetTypeTicketCommand, object>
    {
        private readonly IReportRepository _callHistoryRepository;
        //private readonly IMapper _mapper;
        public GetTypeTicketCommandHandler(IReportRepository callHistoryRepository)
        {
            _callHistoryRepository = callHistoryRepository;
            //_mapper = mapper;
        }

        public async Task<object> Handle(GetTypeTicketCommand request, CancellationToken cancellationToken)
        {
            // var dateCall = request.DateCall;
            Debug.WriteLine("Đã Chạy tới Service");
            var callHistoryList = await _callHistoryRepository.GetTypeTicket(request.DateStart, request.DateEnd);
             
          return callHistoryList;
        }
    }
}
