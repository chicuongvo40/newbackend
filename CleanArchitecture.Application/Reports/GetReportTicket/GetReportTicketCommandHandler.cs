﻿using AutoMapper;
using CleanArchitecture.Application.Schedules.GetSchedule;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Reports.GetReportTicket
{
    public class GetReportTicketCommandHandler : IRequestHandler<GetReportTicketCommand, object>
    {
        private readonly IReportRepository _reportRepository;
        //private readonly IMapper _mapper;

        public GetReportTicketCommandHandler(IReportRepository reportRepository)
        {
            _reportRepository = reportRepository;
           // _mapper = mapper;
        }
        public async Task<object> Handle(GetReportTicketCommand request, CancellationToken cancellationToken)
        {
            var schedule = await _reportRepository.GetTicketReport();
            //var scheduleDTOs = _mapper.Map<List<ScheduleDTO>>(schedule);
            //var op = PageHelper<ScheduleDTO>.Paging(scheduleDTOs, request.page, request.size);
            return schedule;
        }
    }
}
