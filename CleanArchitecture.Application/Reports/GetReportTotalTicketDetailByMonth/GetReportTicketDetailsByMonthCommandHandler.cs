﻿using CleanArchitecture.Application.Reports.GetReportTotalCallDetailbyMonth;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Reports.GetReportTotalTicketDetailByMonth
{
    public class GetReportTicketDetailsByMonthCommandHandler : IRequestHandler<GetReportTicketDetailsByMonthCommand, object>
    {
        private readonly IReportRepository _callHistoryRepository;
        public GetReportTicketDetailsByMonthCommandHandler(IReportRepository callHistoryRepository)
        {
            _callHistoryRepository = callHistoryRepository;
        }

        public async Task<object> Handle(GetReportTicketDetailsByMonthCommand request, CancellationToken cancellationToken)
        {

            var callHistoryList = await _callHistoryRepository.GetTotalTicketDetailByMonth(request.month, request.year);

            return callHistoryList;
        }
    }
}
