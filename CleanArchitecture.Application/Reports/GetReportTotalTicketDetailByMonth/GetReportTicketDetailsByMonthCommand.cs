﻿using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Reports.GetReportTotalTicketDetailByMonth
{
    public class GetReportTicketDetailsByMonthCommand : IRequest<object>, ICommand
    {
        public int month { get; set; }
        public int year { get; set; }
    }
}
