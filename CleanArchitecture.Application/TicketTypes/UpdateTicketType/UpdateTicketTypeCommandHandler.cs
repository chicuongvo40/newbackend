﻿using CleanArchitecture.Application.Schedules.UpdateSchedule;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketTypes.UpdateTicketType
{
    public class UpdateTicketTypeCommandHandler : IRequestHandler<UpdateTicketTypeCommand, Unit>
    {
        private readonly ITicketTypeRepositories _typeRepository;
        public UpdateTicketTypeCommandHandler(ITicketTypeRepositories typeRepository)
        {
            _typeRepository = typeRepository;
        }
        public async Task<Unit> Handle(UpdateTicketTypeCommand request, CancellationToken cancellationToken)
        {
            var type = await _typeRepository.FindByIdAsync(request.Id, cancellationToken);

            if (type == null)
            {
                throw new NotFoundException("TicketType not found.");
            }
            type.TicketTypeName = request.TicketTypeName;
            type.KpiDuration = request.KpiDuration;
            //await _braRepository.UpdateBranchSP(bra);
            _typeRepository.Update(type);
            await _typeRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
