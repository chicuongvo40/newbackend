﻿using CleanArchitecture.Application.Levels.CreateLevel;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketTypes.CreateTicketType
{
    public class CreateTicketTypeCommandValidator : AbstractValidator<CreateTicketTypeCommand>
    {
        public CreateTicketTypeCommandValidator()
        {
            RuleFor(command => command.TicketTypeName)
             .NotEmpty().WithMessage("TicketTypeName  is required");
            RuleFor(command => command.KpiDuration)
             .NotEmpty().WithMessage("KpiDuration name is required");
        }
    }
}
