﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.TicketTypes.CreateTicketType
{
    public class CreateTicketTypeCommand : IRequest<TicketType>, ICommand
    {
        public string TicketTypeName { get; set; }

        public string KpiDuration { get; set; }
    }
}
