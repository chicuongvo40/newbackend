﻿using CleanArchitecture.Application.Schedules.CreateSchedule;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketTypes.CreateTicketType
{
    public class CreateTicketTypeCommandHandler : IRequestHandler<CreateTicketTypeCommand, TicketType>
    {
        private readonly ITicketTypeRepositories _typeRepository;
        public CreateTicketTypeCommandHandler(ITicketTypeRepositories typeRepository)
        {
            _typeRepository = typeRepository;
        }
        public async Task<TicketType> Handle(CreateTicketTypeCommand request, CancellationToken cancellationToken)
        {
            var _type = new TicketType(
               request.TicketTypeName,
               request.KpiDuration
                );
            //await _braRepository.CreateBranchSP(_branch);
            _typeRepository.Add(_type);
            await _typeRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return _type;
        }
    }
}
