﻿using AutoMapper;
using CleanArchitecture.Application.Branchs.GetBranchById;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketTypes.GetTicketTypeById
{
    public class GetTicketTypeByIdCommandHandler : IRequestHandler<GetTicketTypeByIdCommand, TicketTypeDTO>
    {
        private readonly ITicketTypeRepositories _typeRepo;
        private readonly IMapper _mapper;
        public GetTicketTypeByIdCommandHandler(ITicketTypeRepositories typeRepo, IMapper mapper)
        {
            _typeRepo = typeRepo;
            _mapper = mapper;
        }
        public async Task<TicketTypeDTO> Handle(GetTicketTypeByIdCommand request, CancellationToken cancellationToken)
        {
            var typeId = request.TypeId;

            var type = await _typeRepo.FindByIdAsync(typeId, cancellationToken);

            if (type != null)
            {
                var typeDTO = _mapper.Map<TicketTypeDTO>(type);
                return typeDTO;
            }
            throw new NotFoundException($"Branch with ID {typeId} not found");
        }
    }
}
