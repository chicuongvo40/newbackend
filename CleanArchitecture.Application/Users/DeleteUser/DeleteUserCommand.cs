﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Users.DeleteUser
{
    public class DeleteUserCommand : IRequest<Unit>, ICommand
    {
        public DeleteUserCommand(int userid)
        {
            Id = userid;
        }
        public int Id { get; set; }
    }
}
