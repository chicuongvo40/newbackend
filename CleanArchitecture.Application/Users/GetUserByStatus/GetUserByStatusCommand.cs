﻿using CleanArchitecture.Domain.DTO;
using MediatR;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Users.GetUserByStatus
{
    public class GetUserByStatusCommand : IRequest<PagedResults<UserDTO>>, ICommand
    {
      public int id { get; set; }
    }
}
