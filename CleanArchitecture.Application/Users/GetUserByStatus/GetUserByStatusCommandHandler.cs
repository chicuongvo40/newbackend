﻿using AutoMapper;
using CleanArchitecture.Application.Users.GetUserByStatus;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;

namespace CleanArchitecture.Application.Users.GetUserByStatus
{
    public class GetUserByStatusCommandHandler : IRequestHandler<GetUserByStatusCommand, PagedResults<UserDTO>>
    {
        private readonly IUserRepository _userRepo;
        private readonly IMapper _mapper;
        public GetUserByStatusCommandHandler(IUserRepository userRepo, IMapper mapper)
        {
            _userRepo = userRepo;
            _mapper = mapper;
        }
        public async Task<PagedResults<UserDTO>> Handle(GetUserByStatusCommand request, CancellationToken cancellationToken)
        {
            

            var user = await _userRepo.FindByStatus(request.id, cancellationToken);

            if (user != null)
            {
                var userDTO = _mapper.Map<List<UserDTO>>(user);
                var op = PageHelper<UserDTO>.Paging(userDTO, 1, 100);
                return op;
            }

            throw new NotFoundException($"User with not found");
        }
    }
}
