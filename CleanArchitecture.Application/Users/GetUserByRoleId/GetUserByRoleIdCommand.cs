﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Users.GetUserByRoleId
{
    public class GetUserByRoleIdCommand : IRequest<List<UserDTO>>, ICommand
    {
        public int RoleId { get; set; }
    }
}
