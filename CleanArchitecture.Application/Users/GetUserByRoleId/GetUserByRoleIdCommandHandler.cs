﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Users.GetUserByRoleId
{
    public class GetUserByRoleIdCommandHandler : IRequestHandler<GetUserByRoleIdCommand, List<UserDTO>>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public GetUserByRoleIdCommandHandler(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<List<UserDTO>> Handle(GetUserByRoleIdCommand request, CancellationToken cancellationToken)
        {
            var roleId = request.RoleId;
            var users = await _userRepository.FindByRoleIdAsync(roleId, cancellationToken);

            if (users != null && users.Count > 0)
            {
                var userDTOs = _mapper.Map<List<UserDTO>>(users);
                return userDTOs;
            }

            throw new NotFoundException($"No users found with Role ID {roleId}");
        }
    }
}