﻿using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.DeleteCustomer
{
    public class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerCommand, Unit>
    {
        private readonly ICustomerRepositories _cusRepository;


        public DeleteCustomerCommandHandler(ICustomerRepositories cusRepository)
        {
            _cusRepository = cusRepository;
        }


        public async Task<Unit> Handle(DeleteCustomerCommand request, CancellationToken cancellationToken)
        {
            //await _cusRepository.DeleteCustomerSP(request.CustomerId);
            var customer = await _cusRepository.FindByIdAsync(request.CustomerId, cancellationToken);

            if (customer == null)
            {
                throw new NotFoundException("Customer not found.");
            }
                _cusRepository.Remove(customer);
                await _cusRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            
            return Unit.Value;
        }
    }
}
