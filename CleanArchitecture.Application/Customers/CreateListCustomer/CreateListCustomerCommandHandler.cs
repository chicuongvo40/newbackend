﻿using CleanArchitecture.Application.Customers.CreateCustomer;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.CreateListCustomer
{
    public class CreateListCustomerCommandHandler : IRequestHandler<CreateListCustomerCommand, List<Customer>>
    {
        private readonly ICustomerRepositories _cusRepository;


        public CreateListCustomerCommandHandler(ICustomerRepositories cusRepository)
        {
            _cusRepository = cusRepository;
        }


        public async Task<List<Customer>> Handle(CreateListCustomerCommand request, CancellationToken cancellationToken)
        {
            foreach (var customer in request.List)
            {
                _cusRepository.Add(customer);

            }
            await _cusRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return request.List;
        }
    }
}
