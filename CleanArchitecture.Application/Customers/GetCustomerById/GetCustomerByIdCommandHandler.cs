﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.GetCustomerById
{
    public class GetCustomerByIdCommandHandler : IRequestHandler<GetCustomerByIdCommand, CustomerDTO>
    {
        private readonly ICustomerRepositories _cusRepository;
        private readonly IMapper _mapper;

        public GetCustomerByIdCommandHandler(ICustomerRepositories cusRepository, IMapper mapper)
        {
            _cusRepository = cusRepository;
            _mapper = mapper;
        }

        public async Task<CustomerDTO> Handle(GetCustomerByIdCommand request, CancellationToken cancellationToken)
        {
            var customerId = request.CustomerId;

            var customer = await _cusRepository.FindByIdAsync(customerId, cancellationToken);

            if (customer != null)
            {
                Console.WriteLine("aa");
                var customerDTO = _mapper.Map<CustomerDTO>(customer);
                return customerDTO;
            }
            //var customer = await _cusRepository.GetCustomerSP();
            //var customerDTO = _mapper.Map<CustomerDTO>(customer.First());
            //return customerDTO;
            throw new NotFoundException($"Customer with ID {customerId} not found");
        }
    }
}
