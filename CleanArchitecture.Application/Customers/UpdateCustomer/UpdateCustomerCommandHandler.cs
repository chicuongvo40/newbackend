﻿using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.UpdateCustomer
{
    public class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand, Unit>
    {
        private readonly ICustomerRepositories _cusRepository;


        public UpdateCustomerCommandHandler(ICustomerRepositories cusRepository)
        {
            _cusRepository = cusRepository;
        }


        public async Task<Unit> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = await _cusRepository.FindByIdAsync(request.Id, cancellationToken);

            if (customer == null)
            {
                throw new NotFoundException("Customer not found.");
            }
            customer.CustomerLevelId = request.CustomerLevelId;
            customer.SourceId = request.SourceId;
            customer.BranchId = request.BranchId;
            customer.LastName = request.LastName;
            customer.FirstName = request.FirstName;
            customer.PhoneNumber = request.PhoneNumber;
            customer.Status = request.Status;
            customer.Gender = request.Gender;
            customer.Address = request.Address;
            customer.DayOfBirth = request.DayOfBirth;
            customer.DateCreated = request.DateCreated;
            customer.LastEditedTime = request.LastEditedTime;
            customer.Name = request.Name;
            customer.Email = request.Email;

            //await _cusRepository.UpdateCustomerSP(customer);
            _cusRepository.Update(customer);
            await _cusRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}

