﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.UpdateCustomer
{
    public class UpdateCustomerCommandValidator :  AbstractValidator<UpdateCustomerCommand>
    {
        public UpdateCustomerCommandValidator() 
        {
            // RuleFor(command => command.FirstName)
            //.NotEmpty().WithMessage("First name is required")
            //.MaximumLength(50).WithMessage("First name must not exceed 50 characters.")
            //.Matches("^[a-zA-Z ]+$").WithMessage("Branch name should only contain alphabetic characters.");

            //RuleFor(command => command.LastName)
            //    .NotEmpty().WithMessage("Last name is required")
            //    .MaximumLength(50).WithMessage("Last name must not exceed 50 characters.")
            //    .Matches("^[a-zA-Z ]+$").WithMessage("Branch name should only contain alphabetic characters.");

            //RuleFor(command => command.Name)
            //  .NotEmpty().WithMessage("Last name is required")
            //  .MaximumLength(50).WithMessage("Last name must not exceed 50 characters.")
            //  .Matches("^[a-zA-Z ]+$").WithMessage("Branch name should only contain alphabetic characters.");

            //RuleFor(command => command.PhoneNumber)
            //    .NotEmpty().WithMessage("Phone number is required")
            //    .Matches(@"(^(0)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$)").WithMessage("Invalid phone number format");

            //RuleFor(command => command.Address)
            //    .NotEmpty().WithMessage("Address is required")
            //    .MaximumLength(100).WithMessage("Address must not exceed 100 characters.");
        }
    }
}
