﻿using AutoMapper;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.GetCustomer
{
    public class GetCustomerCommandHandler : IRequestHandler<GetCustomerCommand, PagedResults<CustomerDTO>>
    {
        private readonly ICustomerRepositories _cusRepository;
        private readonly IMapper _mapper;

        public GetCustomerCommandHandler(ICustomerRepositories cusRepository, IMapper mapper)
        {
            _cusRepository = cusRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<CustomerDTO>> Handle(GetCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = await _cusRepository.GetAllCustomersFromBranch(request.id);
            var customerDTO = _mapper.Map<List<CustomerDTO>>(customer);
            var op = PageHelper<CustomerDTO>.Paging(customerDTO, request.page, request.size);
            return op;
        }
    }
}
