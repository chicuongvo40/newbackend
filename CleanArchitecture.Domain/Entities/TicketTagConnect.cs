﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class TicketTagConnect
{
    public int Id { get; set; }

    public int TicketId { get; set; }

    public int TicketTagId { get; set; }

    public virtual Ticket? Ticket { get; set; }

    public virtual TicketTag? TicketTag { get; set; }
}
