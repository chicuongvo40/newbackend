﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class TicketLevel
{
    public TicketLevel()
    {
    }

    public TicketLevel(string levelName)
    {
        LevelName = levelName;
    }

    public int Id { get; set; }

    public string? LevelName { get; set; }

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
}
