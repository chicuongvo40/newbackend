﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class CustomerLevel
{
    public CustomerLevel()
    {
    }

    public CustomerLevel(string levelName)
    {
        LevelName = levelName;
    }

    public int Id { get; set; }

    public string? LevelName { get; set; }

    public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();
}
