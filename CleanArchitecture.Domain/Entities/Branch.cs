﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Branch
{
    public Branch()
    {
    }

    public Branch(int odsId, string address, string branchName, string email, string callNumber)
    {
        OdsId = odsId;
        Address = address;
        BranchName = branchName;
        Email = email;
        CallNumber = callNumber;
    }

    public int Id { get; set; }

    public int? OdsId { get; set; }

    public string? Address { get; set; }

    public string? BranchName { get; set; }

    public string? Email { get; set; }

    public string? CallNumber { get; set; }

    public virtual ICollection<Contract> Contracts { get; set; } = new List<Contract>();

    public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();

    public virtual Od? Ods { get; set; }
    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
    public virtual ICollection<User> Users { get; set; } = new List<User>();
}
