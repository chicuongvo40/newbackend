﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Log
{
    public Log()
    {
    }

    public Log(int ticketId, string note, DateTime createdDate, string code, string imgUrl)
    {
        TicketId = ticketId;
        Note = note;
        CreatedDate = createdDate;
        Code = code;
        ImgUrl = imgUrl;
    }

    public int Id { get; set; }

    public int? TicketId { get; set; }

    public string? Note { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string? Code { get; set; }

    public string? ImgUrl { get; set; }

    public virtual Ticket? Ticket { get; set; }
}
