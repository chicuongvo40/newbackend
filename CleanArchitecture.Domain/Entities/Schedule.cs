﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Schedule
{
    public Schedule() { }
    public Schedule(int customerId, int staffId, string tittle, string status, string note, DateTime createdDate, DateTime meetTime, DateTime lastEditedTime)
    {
        CustomerId = customerId;
        StaffId = staffId;
        Tittle = tittle;
        Status = status;
        Note = note;
        CreatedDate = createdDate;
        MeetTime = meetTime;
        LastEditedTime = lastEditedTime;
    }

    public int Id { get; set; }

    public int? CustomerId { get; set; }

    public int? StaffId { get; set; }

    public string? Tittle { get; set; }

    public string? Status { get; set; }

    public string? Note { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? MeetTime { get; set; }

    public DateTime? LastEditedTime { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual User? Staff { get; set; }
}
