﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Source
{
    public Source()
    {
    }

    public Source(string sourceName)
    {
        SourceName = sourceName;
    }

    public int Id { get; set; }

    public string? SourceName { get; set; }

    public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();
}
