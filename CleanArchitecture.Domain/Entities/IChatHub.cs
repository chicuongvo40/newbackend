﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Entities
{
        public interface IChatHub
        {
            Task SendMessage(NotifyMessage message);
        }
    
}