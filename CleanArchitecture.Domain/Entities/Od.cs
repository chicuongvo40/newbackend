﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Od
{
    public Od()
    {
    }

    public Od(string odsBranch, string odsPassword)
    {
        OdsBranch = odsBranch;
        OdsPassword = odsPassword;
    }

    public int Id { get; set; }

    public string? OdsBranch { get; set; }

    public string? OdsPassword { get; set; }

    public virtual ICollection<Branch> Branches { get; set; } = new List<Branch>();
}
