﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ITicketTypeRepositories : IEFRepository<TicketType, TicketType>
    {
        Task<TicketType?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<TicketType>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
    }
}
