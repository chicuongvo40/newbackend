﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ITicketTagRepositories : IEFRepository<TicketTag, TicketTag>
    {
        Task<TicketTag?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<TicketTag>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
    }
}
