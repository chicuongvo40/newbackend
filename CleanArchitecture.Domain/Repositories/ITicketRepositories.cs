﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ITicketRepositories : IEFRepository<Ticket, Ticket>
    {

        Task<List<Ticket>> FindByBranch(int id, CancellationToken cancellationToken = default);
        Task<Ticket?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<Ticket>> FindByStatusIdAsync(int statusId, CancellationToken cancellationToken = default);
        Task<List<Ticket>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<List<Ticket>> FindByCustomerIdAsync(int customerId, CancellationToken cancellationToken = default);
        Task<List<Ticket>> SearchDayAsync(DateTime date, CancellationToken cancellationToken = default);
       
        Task<List<Ticket>> FindByUserIdAsync(int customerId, CancellationToken cancellationToken = default);
    }
}
