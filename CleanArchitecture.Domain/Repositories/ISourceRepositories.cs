﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ISourceRepositories : IEFRepository<Source, Source>
    {
        Task<Source?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<Source>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
    }
}