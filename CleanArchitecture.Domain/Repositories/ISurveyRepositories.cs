﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ISurveyRepositories : IEFRepository<Survey, Survey>
    {
        Task<Survey?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<Survey>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<List<Survey>> GetSurveySP(CancellationToken cancellationToken = default);
        Task<List<Survey>> FindByCustomerIdAsync(int customerId, CancellationToken cancellationToken = default);
    }
}
