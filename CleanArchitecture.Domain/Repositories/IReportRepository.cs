﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface IReportRepository
    {
        Task<object> GetTicketReport();
        Task<object> GetTotalCallDetail(DateTime startDate, DateTime endDate);
        Task<object> GetTotalCallDetailByMonth(int month, int year);
        Task<object> GetTotalTicket(DateTime startDate, DateTime endDate);
        Task<object> GetTotalTicketDetailByMonth(int month, int year);

        Task<object> GetTypeTicket(DateTime startDate, DateTime endDate);
    }
}
