﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ILogRepository : IEFRepository<Log, Log>
    {
        Task<Log?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<Log>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<Log?> GetByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<Log>> FindByTicketIdAsync(int ticketId, CancellationToken cancellationToken = default);

        Task<List<Log>> GetLogSP(CancellationToken cancellationToken = default);

        Task<List<Log>> GetLogByUserIdSP(int id, CancellationToken cancellationToken = default);
        Task<List<Log>> SearchTineLogs(DateTime timeStart, DateTime timeEnd, CancellationToken cancellationToken = default);
    }
}

