﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface IContractRepositories : IEFRepository<Contract, Contract>
    {
        Task<Contract?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<Contract>> FindByCustomerIdAsync(int customerId, CancellationToken cancellationToken = default);
        Task<List<Contract>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<List<Contract>> FindByPhoneAsync(string phone, CancellationToken cancellationToken = default);
    }
}
