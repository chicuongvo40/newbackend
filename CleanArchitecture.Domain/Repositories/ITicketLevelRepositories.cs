﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ITicketLevelRepositories : IEFRepository<TicketLevel, TicketLevel>
    {
        Task<TicketLevel?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<TicketLevel>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<TicketLevel?> GetByIdAsync(int id, CancellationToken cancellationToken = default);
   
    }
}

