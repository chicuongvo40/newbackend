﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ICustomerLevelRepositories : IEFRepository<CustomerLevel, CustomerLevel>
    {
        Task<CustomerLevel?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<CustomerLevel>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
    }
}