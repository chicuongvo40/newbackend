﻿using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ICustomerRepositories : IEFRepository<Customer, Customer>
    {
        Task<List<Customer>> GetAllCustomersFromBranch(int id, CancellationToken cancellationToken = default);
        Task<Customer?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<Customer?> Login(string username, string password, CancellationToken cancellationToken = default);
        Task<List<Customer>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<Customer?> FindByIdsCustomer(int? ids, CancellationToken cancellationToken = default);
        Task<List<Customer>> FindAllCustomer(CancellationToken cancellationToken = default);
        Task<Customer?> FindByEmailAsync(string email, CancellationToken cancellationToken = default);
        Task<Customer?> FindByPhoneNumberAsync(string phoneNumber, CancellationToken cancellationToken = default);
        Task<List<Customer>> GetCustomerSP(CancellationToken cancellationToken = default);
        Task<CustomerDTO> GetCustomerById(int Id);
        Task<List<Customer>> SearchCustomersByNameAsync(string name, CancellationToken cancellationToken = default);
        Task<List<Customer>> GetCustomerByIdSP(int id, CancellationToken cancellationToken = default);
    }
}
