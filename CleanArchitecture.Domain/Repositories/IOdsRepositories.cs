﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface IOdsRepositories : IEFRepository<Od, Od>
    {
        Task<Od?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<Od>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<List<Od>> GetOdsSP(CancellationToken cancellationToken = default);
        Task<List<Od>> GetOdsByIdSP(int id, CancellationToken cancellationToken = default);

    }
}
