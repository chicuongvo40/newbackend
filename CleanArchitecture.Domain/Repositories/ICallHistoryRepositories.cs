﻿using CleanArchitecture.Api.Models;
using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ICallHistoryRepositories : IEFRepository<CallHistory, CallHistory>
    {
        Task<CallHistory?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<CallHistory>> FindByCustomerIdAsync(int customerId, CancellationToken cancellationToken = default);
        Task<List<CallHistory>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<List<CallHistory>> SearchDayAsync(DateTime date, CancellationToken cancellationToken = default);
        Task<List<CallHistory>> SearchByDirection(string direction, CancellationToken cancellationToken);
        Task<List<CallHistory>> FindByPhoneAsync(string phone, CancellationToken cancellationToken = default);

        Task<List<CallHistory>> GetCustomerByUserId(int id, CancellationToken cancellationToken = default);

    }
}
