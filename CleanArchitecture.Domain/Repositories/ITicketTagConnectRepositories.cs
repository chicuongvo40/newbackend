﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ITicketTagConnectRepositories : IEFRepository<TicketTagConnect, TicketTagConnect>
    {
        Task<TicketTagConnect?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<TicketTagConnect>> FindByTicketId(int ticketId, CancellationToken cancellationToken = default);
        Task<List<TicketTagConnect>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
    }
}

