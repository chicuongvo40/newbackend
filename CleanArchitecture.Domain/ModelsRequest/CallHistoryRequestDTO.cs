﻿namespace CleanArchitecture.Api.Models
{
    public class CallHistoryRequestDTO
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string Key { get; set; }
    }
}
