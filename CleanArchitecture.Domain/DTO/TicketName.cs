﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class TicketName
    {
         public int Id { get; set; }
        public int BranchId { get; set; }
        public string? Note { get; set; }
        public CustomerDTO CustomerId { get; set; }
        public string TicketTypeId { get; set; }
        public string LevelId { get; set; }
        public string TicketStatusId { get; set; }
        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }
        public int? AssignedUserId { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public List<string> Tags { get; set; }
    }
}
