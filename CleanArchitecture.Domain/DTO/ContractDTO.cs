﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class ContractDTO
    {
        public int Id { get; set; }

        public int? CustomerId { get; set; }

        public string? ContractType { get; set; }

        public string? TermsAndConditions { get; set; }

        public DateTime? TimeStart { get; set; }

        public DateTime? TimeEnd { get; set; }

        public DateTime? LastEditedTime { get; set; }

        public string? Code { get; set; }

        public string? Title { get; set; }

        public string? Address { get; set; }

        public string? CallNumber { get; set; }

        public int? BranchId { get; set; }

    }
}
