﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class TicketWithTagDTO
    {
        public List<TicketName> StatusOne { get; set; }

        public List<TicketName> StatusTwo { get; set; }
        public List<TicketName> StatusThree { get; set; }
        public List<TicketName> StatusFour { get; set; }

    }
}
