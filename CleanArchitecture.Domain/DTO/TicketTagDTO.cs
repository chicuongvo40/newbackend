﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class TicketTagDTO
    {
        public int Id { get; set; }

        public string? TagName { get; set; }
    }
}
