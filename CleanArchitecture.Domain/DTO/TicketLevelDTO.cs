﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class TicketLevelDTO
    {
        public int Id { get; set; }

        public string? LevelName { get; set; }
    }
}
