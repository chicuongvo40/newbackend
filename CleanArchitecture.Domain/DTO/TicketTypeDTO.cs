﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class TicketTypeDTO
    {
        public int Id { get; set; }

        public string? TicketTypeName { get; set; }

        public string? KpiDuration { get; set; }
    }
}
