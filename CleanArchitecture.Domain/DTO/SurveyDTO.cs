﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class SurveyDTO
    {
        public int Id { get; set; }

        public string? SatisfactionRating { get; set; }

        public string? Comment { get; set; }

        public DateTime? SurveyDate { get; set; }

        public int? CustomerId { get; set; }
    }
}
