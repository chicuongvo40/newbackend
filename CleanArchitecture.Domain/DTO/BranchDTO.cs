﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class BranchDTO
    {
        public int Id { get; set; }

        public int? OdsId { get; set; }

        public string? Address { get; set; }

        public string? BranchName { get; set; }

        public string? Email { get; set; }

        public string? CallNumber { get; set; }
    }
}
