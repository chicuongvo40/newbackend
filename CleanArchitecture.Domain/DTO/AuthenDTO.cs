﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class AuthenDTO
    {   
        public string Name { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string JWT { get; set; }
        public int BranchId { get; set; }

        public string BranchName { get; set; }
        public  int id { get; set; }
    }
}
