﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class ReportTicket
    {
        public string Name { get; set; } = "A";
        public int Success { get; set; }

        public int Time { get; set; }
        public int Fail { get; set; }

      

    }
}
