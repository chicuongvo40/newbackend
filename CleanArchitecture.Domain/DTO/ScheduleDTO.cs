﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class ScheduleDTO
    {
        public int Id { get; set; }

        public int? CustomerId { get; set; }

        public int? StaffId { get; set; }

        public string? Tittle { get; set; }

        public string? Status { get; set; }

        public string? Note { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? MeetTime { get; set; }

        public DateTime? LastEditedTime { get; set; }
    }
}
