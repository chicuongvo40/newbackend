﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class ProgressDTO
    {
        public int Id { get; set; }

        public int? IdTicket { get; set; }

        public int? IdStaff { get; set; }

        public DateTime? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public string? Image { get; set; }

        public string? Note { get; set; }
    }
}
