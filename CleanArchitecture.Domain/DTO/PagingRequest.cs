﻿

namespace CleanArchitecture.Domain.DTO
{
    public class PagingRequest
    {
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = 2;
        public string KeySearch { get; set; } = "";
        public string SearchBy { get; set; } = "";
        public string ColName { get; set; } = "Id";
    }
}