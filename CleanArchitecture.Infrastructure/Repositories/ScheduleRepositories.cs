﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class ScheduleRepositories : RepositoryBase<Schedule, Schedule, NewtelecallbeContext>, IScheduleRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public ScheduleRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<Schedule?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<Schedule>> FindByCustomerIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.CustomerId == id, cancellationToken);
        }
        public async Task<List<Schedule>> SearchByTittle(string tittle, CancellationToken cancellationToken)
        {
            return await FindAllAsync(x => x.Tittle == tittle, cancellationToken);
        }
        public async Task<List<Schedule>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
    }
}
