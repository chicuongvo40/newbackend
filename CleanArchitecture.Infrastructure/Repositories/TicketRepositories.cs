﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class TicketRepositories : RepositoryBase<Ticket, Ticket, NewtelecallbeContext>, ITicketRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        private readonly IMapper _mapper;
        public TicketRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Ticket?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }
        public async Task<List<Ticket>> FindByCustomerIdAsync(int customerId, CancellationToken cancellationToken = default)
        {
            Debug.WriteLine("Đây là id của customer trong repo " + customerId.ToString());
            return await _dbContext.Tickets
                .Where(u => u.CustomerId == customerId)
                .ToListAsync(cancellationToken);
        }

        public async Task<List<Ticket>> FindByBranch(int id, CancellationToken cancellationToken = default)
        {
          
            return await _dbContext.Tickets
                .Where(u => u.BranchId == id)
                .ToListAsync(cancellationToken);
        }
        public async Task<List<Ticket>> FindByUserIdAsync(int customerId, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Tickets
                .Where(u => u.AssignedUserId == customerId)
                .ToListAsync(cancellationToken);
        }
        public async Task<List<Ticket>> FindByStatusIdAsync(int statusId, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Tickets
                .Where(u => u.TicketStatusId == statusId)
                .ToListAsync(cancellationToken);
        }
        public async Task<List<Ticket>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<List<Ticket>> SearchDayAsync(DateTime date, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Tickets
                .Where(ch => ch.CreatedDate == date)
                .ToListAsync(cancellationToken);
        }
    }
}
