﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class LogRepository : RepositoryBase<Log, Log, NewtelecallbeContext>, ILogRepository
    {
        private readonly NewtelecallbeContext _dbContext;
        public LogRepository(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<Log?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<Log>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<Log?> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindByIdAsync(id, cancellationToken);
        }
        public async Task<List<Log>> GetLogSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Log>().FromSqlInterpolated($"Exec Log_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<List<Log>> FindByTicketIdAsync(int ticketId, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Logs
                .Where(u => u.TicketId == ticketId)
                .ToListAsync(cancellationToken);
        }

        public async Task<List<Log>> GetLogByUserIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Log>().FromSqlInterpolated($"Exec Log_SP @Operation=\"USERID\",@UserId={id}").ToListAsync(cancellationToken);
        }
        public async Task<List<Log>> SearchTineLogs(DateTime timeStart, DateTime timeEnd, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Log>().FromSqlInterpolated($"Exec Log_SP @Operation=\"DATE\",@TimeStart= {timeStart}, @TimeEnd= {timeEnd}").ToListAsync(cancellationToken);
        }
    }
}
