﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class TicketLevelRepositories : RepositoryBase<TicketLevel, TicketLevel, NewtelecallbeContext>, ITicketLevelRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public TicketLevelRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<TicketLevel?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<TicketLevel>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<TicketLevel?> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindByIdAsync(id, cancellationToken);
        }
       
    }
}
