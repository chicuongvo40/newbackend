﻿using AutoMapper;
using Castle.Core.Resource;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;

using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class CustomerRepositories : RepositoryBase<Customer, Customer, NewtelecallbeContext>, ICustomerRepositories
    {

        private readonly NewtelecallbeContext _dbContext;
        private readonly IMapper _mapper;
        public CustomerRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Customer?> Login(string username, string password, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Email == username && x.Email == password, cancellationToken);
        }

        public async Task<Customer?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<Customer>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }

        public async Task<Customer?> FindByIdsCustomer(int? ids, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == ids, cancellationToken);
        }

       
        public async Task<List<Customer>> FindAllCustomer(CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(cancellationToken);
        }
        public async Task<Customer?> FindByEmailAsync(string number, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.PhoneNumber == number, cancellationToken);
        }
        public async Task<List<Customer>> GetCustomerSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Customer>().FromSqlInterpolated($"Exec Customer_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<List<Customer>> SearchCustomersByNameAsync(string name, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Customers
                 .Where(u => u.Name == name)
                 .ToListAsync(cancellationToken);
        }

        public async Task<List<Customer>> GetAllCustomersFromBranch(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Customers
                 .Where(u => u.BranchId == id)
                 .ToListAsync(cancellationToken);
        }

        public async Task<Customer?> FindByPhoneNumberAsync(string phoneNumber, CancellationToken cancellationToken = default)
        {
            // Implement the logic to find a customer by phone number
            return await FindAsync(c => c.PhoneNumber == phoneNumber, cancellationToken);
        }
        public async Task<List<Customer>> GetCustomerByIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Customer>().FromSqlInterpolated($"Exec Customer_SP @Operation=\"SEARCH_ID\",@Id={id}").ToListAsync(cancellationToken);
        }
        public async Task<CustomerDTO> GetCustomerById(int Id)
        {
            var tickets = await _dbContext.Customers.FirstOrDefaultAsync(x => x.Id == Id ); // Lấy danh sách tất cả các vé từ cơ sở dữ liệu
            var ticketDTO = _mapper.Map<CustomerDTO>(tickets);
            return ticketDTO; // Trả về kết quả
        }

    }

}
