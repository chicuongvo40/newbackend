﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class BranchRepositories : RepositoryBase<Branch, Branch, NewtelecallbeContext>, IBranchRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public BranchRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<Branch?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<Branch>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<Branch?> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindByIdAsync(id, cancellationToken);
        }
        public async Task<List<Branch>> GetBranchSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Branch>().FromSqlInterpolated($"Exec Branch_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
       
    }
}