﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class OdsRepositories : RepositoryBase<Od, Od, NewtelecallbeContext>, IOdsRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public OdsRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<Od?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<Od>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<List<Od>> GetOdsSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Od>().FromSqlInterpolated($"Exec Ods_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<List<Od>> GetOdsByIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Od>().FromSqlInterpolated($"Exec Ods_SP @Operation=\"SEARCH\",@Id={id}").ToListAsync(cancellationToken);
        }
    }
}
