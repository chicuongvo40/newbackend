﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class Od
{
    public int Id { get; set; }

    public string? OdsBranch { get; set; }

    public string? OdsPassword { get; set; }

    public virtual ICollection<Branch> Branches { get; set; } = new List<Branch>();
}
