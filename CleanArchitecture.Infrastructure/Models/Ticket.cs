﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class Ticket
{
    public int Id { get; set; }

    public int? BranchId { get; set; }

    public string? Note { get; set; }

    public int? CustomerId { get; set; }

    public int? TicketTypeId { get; set; }

    public int? LevelId { get; set; }

    public int? TicketStatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string? CreatedBy { get; set; }

    public int? AssignedUserId { get; set; }

    public string? Code { get; set; }

    public string? Title { get; set; }

    public virtual User? AssignedUser { get; set; }

    public virtual Branch? Branch { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual TicketLevel? Level { get; set; }

    public virtual ICollection<Log> Logs { get; set; } = new List<Log>();

    public virtual TicketStatus? TicketStatus { get; set; }

    public virtual ICollection<TicketTagConnect> TicketTagConnects { get; set; } = new List<TicketTagConnect>();

    public virtual TicketType? TicketType { get; set; }
}
