﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class Log
{
    public int Id { get; set; }

    public int? TicketId { get; set; }

    public string? Note { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string? Code { get; set; }

    public string? ImgUrl { get; set; }

    public virtual Ticket? Ticket { get; set; }
}
