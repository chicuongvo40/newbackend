﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class CustomerLevel
{
    public int Id { get; set; }

    public string? LevelName { get; set; }

    public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();
}
