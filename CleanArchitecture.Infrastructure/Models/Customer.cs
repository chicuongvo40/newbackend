﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class Customer
{
    public int Id { get; set; }

    public int? CustomerLevelId { get; set; }

    public int? SourceId { get; set; }

    public int? BranchId { get; set; }

    public string? LastName { get; set; }

    public string? FirstName { get; set; }

    public string? PhoneNumber { get; set; }

    public string? Status { get; set; }

    public string? Gender { get; set; }

    public string? Address { get; set; }

    public DateTime? DayOfBirth { get; set; }

    public DateTime? DateCreated { get; set; }

    public DateTime? LastEditedTime { get; set; }

    public string? Name { get; set; }

    public string? Email { get; set; }

    public virtual Branch? Branch { get; set; }

    public virtual ICollection<CallHistory> CallHistories { get; set; } = new List<CallHistory>();

    public virtual ICollection<Contract> Contracts { get; set; } = new List<Contract>();

    public virtual CustomerLevel? CustomerLevel { get; set; }

    public virtual ICollection<Schedule> Schedules { get; set; } = new List<Schedule>();

    public virtual Source? Source { get; set; }

    public virtual ICollection<Survey> Surveys { get; set; } = new List<Survey>();

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
}
