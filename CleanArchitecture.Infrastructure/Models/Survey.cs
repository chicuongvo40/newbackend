﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class Survey
{
    public int Id { get; set; }

    public string? SatisfactionRating { get; set; }

    public string? Comment { get; set; }

    public DateTime? SurveyDate { get; set; }

    public int? CustomerId { get; set; }

    public virtual Customer? Customer { get; set; }
}
