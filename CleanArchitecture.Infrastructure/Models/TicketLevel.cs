﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class TicketLevel
{
    public int Id { get; set; }

    public string? LevelName { get; set; }

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
}
