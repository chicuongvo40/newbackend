﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class Schedule
{
    public int Id { get; set; }

    public int? CustomerId { get; set; }

    public int? StaffId { get; set; }

    public string? Tittle { get; set; }

    public string? Status { get; set; }

    public string? Note { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? MeetTime { get; set; }

    public DateTime? LastEditedTime { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual User? Staff { get; set; }
}
