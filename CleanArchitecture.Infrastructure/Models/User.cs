﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class User
{
    public int Id { get; set; }

    public int? BranchId { get; set; }

    public int? RoleId { get; set; }

    public string? LastName { get; set; }

    public string? FirstName { get; set; }

    public string? UserName { get; set; }

    public string? Password { get; set; }

    public string? Status { get; set; }

    public string? Gender { get; set; }

    public string? Address { get; set; }

    public DateTime? DayOfBirth { get; set; }

    public DateTime? CreatedDate { get; set; }

    public virtual Branch? Branch { get; set; }

    public virtual ICollection<CallHistory> CallHistories { get; set; } = new List<CallHistory>();

    public virtual Role? Role { get; set; }

    public virtual ICollection<Schedule> Schedules { get; set; } = new List<Schedule>();

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
}
