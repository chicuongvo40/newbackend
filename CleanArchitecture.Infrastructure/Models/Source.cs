﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class Source
{
    public int Id { get; set; }

    public string? SourceName { get; set; }

    public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();
}
