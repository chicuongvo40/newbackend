﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class TicketType
{
    public int Id { get; set; }

    public string? TicketTypeName { get; set; }

    public string? KpiDuration { get; set; }

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
}
