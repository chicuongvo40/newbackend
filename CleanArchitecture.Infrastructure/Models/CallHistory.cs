﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Models;

public partial class CallHistory
{
    public int Id { get; set; }

    public string? CallNumber { get; set; }

    public int? UserId { get; set; }

    public int? CustomerId { get; set; }

    public string? RealTimeCall { get; set; }

    public string? StatusCall { get; set; }

    public DateTime? DateCall { get; set; }

    public string? RecordLink { get; set; }

    public string? Direction { get; set; }

    public string? TotalTimeCall { get; set; }

    public string? Code { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual User? User { get; set; }
}
