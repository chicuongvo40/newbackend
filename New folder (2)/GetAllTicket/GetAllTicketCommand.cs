﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetAllTicket
{
    public class GetAllTicketCommand : IRequest<PagedResults<Ticket>>, ICommand
    {
        public int page { get; set; }

        public int size { get; set; }
    }
}
