USE [master]
GO
/****** Object:  Database [newtelecallbe]    Script Date: 5/6/2024 2:57:53 PM ******/
CREATE DATABASE [newtelecallbe]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'newtelecallbe', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.CHICUONG\MSSQL\DATA\newtelecallbe.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'newtelecallbe_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.CHICUONG\MSSQL\DATA\newtelecallbe_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [newtelecallbe] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [newtelecallbe].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [newtelecallbe] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [newtelecallbe] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [newtelecallbe] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [newtelecallbe] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [newtelecallbe] SET ARITHABORT OFF 
GO
ALTER DATABASE [newtelecallbe] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [newtelecallbe] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [newtelecallbe] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [newtelecallbe] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [newtelecallbe] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [newtelecallbe] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [newtelecallbe] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [newtelecallbe] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [newtelecallbe] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [newtelecallbe] SET  DISABLE_BROKER 
GO
ALTER DATABASE [newtelecallbe] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [newtelecallbe] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [newtelecallbe] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [newtelecallbe] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [newtelecallbe] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [newtelecallbe] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [newtelecallbe] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [newtelecallbe] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [newtelecallbe] SET  MULTI_USER 
GO
ALTER DATABASE [newtelecallbe] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [newtelecallbe] SET DB_CHAINING OFF 
GO
ALTER DATABASE [newtelecallbe] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [newtelecallbe] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [newtelecallbe] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [newtelecallbe] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [newtelecallbe] SET QUERY_STORE = ON
GO
ALTER DATABASE [newtelecallbe] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [newtelecallbe]
GO
/****** Object:  Schema [HangFire]    Script Date: 5/6/2024 2:57:53 PM ******/
CREATE SCHEMA [HangFire]
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branch](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OdsId] [int] NULL,
	[Address] [nvarchar](255) NULL,
	[BranchName] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[CallNumber] [nvarchar](255) NULL,
 CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CallHistory]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CallHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CallNumber] [nvarchar](255) NULL,
	[UserId] [int] NULL,
	[CustomerId] [int] NULL,
	[RealTimeCall] [nvarchar](255) NULL,
	[StatusCall] [nvarchar](255) NULL,
	[DateCall] [datetime] NULL,
	[RecordLink] [nvarchar](255) NULL,
	[Direction] [nvarchar](255) NULL,
	[TotalTimeCall] [nvarchar](255) NULL,
	[Code] [nvarchar](255) NULL,
 CONSTRAINT [PK_CallHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contract]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contract](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[ContractType] [nvarchar](255) NULL,
	[TermsAndConditions] [nvarchar](255) NULL,
	[TimeStart] [datetime] NULL,
	[TimeEnd] [datetime] NULL,
	[LastEditedTime] [datetime] NULL,
	[Code] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[CallNumber] [nvarchar](255) NULL,
	[BranchId ] [int] NULL,
 CONSTRAINT [PK_Contract] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerLevelId] [int] NULL,
	[SourceId] [int] NULL,
	[BranchId] [int] NULL,
	[LastName] [nvarchar](255) NULL,
	[FirstName] [nvarchar](255) NULL,
	[PhoneNumber] [nvarchar](255) NULL,
	[Status] [nvarchar](255) NULL,
	[Gender ] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[DayOfBirth] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[LastEditedTime] [datetime] NULL,
	[Name] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerLevel]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerLevel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LevelName] [nvarchar](255) NULL,
 CONSTRAINT [PK_CustomerLevel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Log]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TicketId] [int] NULL,
	[Note] [nvarchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[Code] [nvarchar](255) NULL,
	[ImgUrl] [nvarchar](255) NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ODS]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ODS](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OdsBranch] [nvarchar](255) NULL,
	[OdsPassword] [nvarchar](255) NULL,
 CONSTRAINT [PK_ODS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Schedule]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[StaffId] [int] NULL,
	[Tittle] [nvarchar](255) NULL,
	[Status] [nvarchar](255) NULL,
	[Note] [nvarchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[MeetTime] [datetime] NULL,
	[LastEditedTime] [datetime] NULL,
 CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Source]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Source](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SourceName] [nvarchar](255) NULL,
 CONSTRAINT [PK_Source] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Survey]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Survey](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SatisfactionRating] [nvarchar](255) NULL,
	[Comment] [nvarchar](255) NULL,
	[SurveyDate] [datetime] NULL,
	[CustomerId] [int] NULL,
 CONSTRAINT [PK_Survey] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ticket]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ticket](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Branch_Id] [int] NULL,
	[Note] [nvarchar](255) NULL,
	[CustomerId] [int] NULL,
	[TicketTypeId] [int] NULL,
	[LevelId] [int] NULL,
	[TicketStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](255) NULL,
	[AssignedUserId] [int] NULL,
	[Code] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
 CONSTRAINT [PK_Ticket] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketLevel]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketLevel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LevelName] [nvarchar](255) NULL,
 CONSTRAINT [PK_TicketLevel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketStatus]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StatusName] [nvarchar](255) NULL,
 CONSTRAINT [PK_TicketStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketTag]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketTag](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TagName] [nvarchar](255) NULL,
 CONSTRAINT [PK_TicketTag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketTagConnect]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketTagConnect](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TicketId] [int] NULL,
	[TicketTagId] [int] NULL,
 CONSTRAINT [PK_TicketTagConnect] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketType]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TicketTypeName] [nvarchar](255) NULL,
	[KpiDuration] [nvarchar](255) NULL,
 CONSTRAINT [PK_TicketType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NULL,
	[RoleId] [int] NULL,
	[LastName] [nvarchar](255) NULL,
	[FirstName] [nvarchar](255) NULL,
	[UserName] [nvarchar](255) NULL,
	[Password] [nvarchar](255) NULL,
	[Status] [nvarchar](255) NULL,
	[Gender ] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[DayOfBirth] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Branch] ON 

INSERT [dbo].[Branch] ([Id], [OdsId], [Address], [BranchName], [Email], [CallNumber]) VALUES (1, 1, N'a', N'a', N'a', N'a')
INSERT [dbo].[Branch] ([Id], [OdsId], [Address], [BranchName], [Email], [CallNumber]) VALUES (7, 1, N'b', N'b', N'b', N'b')
INSERT [dbo].[Branch] ([Id], [OdsId], [Address], [BranchName], [Email], [CallNumber]) VALUES (8, 1, N'HCM', N'string', N'string', N'string')
SET IDENTITY_INSERT [dbo].[Branch] OFF
GO
SET IDENTITY_INSERT [dbo].[Contract] ON 

INSERT [dbo].[Contract] ([Id], [CustomerId], [ContractType], [TermsAndConditions], [TimeStart], [TimeEnd], [LastEditedTime], [Code], [Title], [Address], [CallNumber], [BranchId ]) VALUES (1, 3, N'a', N'string', CAST(N'2024-05-05T16:09:37.590' AS DateTime), CAST(N'2024-05-05T16:09:37.590' AS DateTime), CAST(N'2024-05-05T16:09:37.590' AS DateTime), N'string', N'string', N'string', N'string', 1)
INSERT [dbo].[Contract] ([Id], [CustomerId], [ContractType], [TermsAndConditions], [TimeStart], [TimeEnd], [LastEditedTime], [Code], [Title], [Address], [CallNumber], [BranchId ]) VALUES (3, 3, N'string', N'string', CAST(N'2024-05-05T16:34:37.373' AS DateTime), CAST(N'2024-05-05T16:34:37.373' AS DateTime), CAST(N'2024-05-05T16:34:37.373' AS DateTime), N'string', N'string', N'string', N'string', 1)
SET IDENTITY_INSERT [dbo].[Contract] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([Id], [CustomerLevelId], [SourceId], [BranchId], [LastName], [FirstName], [PhoneNumber], [Status], [Gender ], [Address], [DayOfBirth], [DateCreated], [LastEditedTime], [Name], [Email]) VALUES (3, 1, 1, 1, N'Cuong', N'Vo', N'0938929584', N'Onl', N'string', N'HCM', CAST(N'2024-05-05T15:31:39.193' AS DateTime), CAST(N'2024-05-05T15:31:39.193' AS DateTime), CAST(N'2024-05-05T15:31:39.193' AS DateTime), N'cuongvo', N'cuongvo@gmail.com')
INSERT [dbo].[Customer] ([Id], [CustomerLevelId], [SourceId], [BranchId], [LastName], [FirstName], [PhoneNumber], [Status], [Gender ], [Address], [DayOfBirth], [DateCreated], [LastEditedTime], [Name], [Email]) VALUES (6, 2, 4, 1, N'string', N'string', N'0909382564', N'string', N'string', N'string', CAST(N'2024-05-05T17:06:52.563' AS DateTime), CAST(N'2024-05-05T17:06:52.563' AS DateTime), CAST(N'2024-05-05T17:06:52.563' AS DateTime), N'string', N'string')
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[CustomerLevel] ON 

INSERT [dbo].[CustomerLevel] ([Id], [LevelName]) VALUES (1, N'string')
INSERT [dbo].[CustomerLevel] ([Id], [LevelName]) VALUES (2, N'Company')
SET IDENTITY_INSERT [dbo].[CustomerLevel] OFF
GO
SET IDENTITY_INSERT [dbo].[ODS] ON 

INSERT [dbo].[ODS] ([Id], [OdsBranch], [OdsPassword]) VALUES (1, N'a', N'a')
INSERT [dbo].[ODS] ([Id], [OdsBranch], [OdsPassword]) VALUES (2, N'C', N's')
INSERT [dbo].[ODS] ([Id], [OdsBranch], [OdsPassword]) VALUES (4, N'string', N'string')
INSERT [dbo].[ODS] ([Id], [OdsBranch], [OdsPassword]) VALUES (6, N'a', N'a')
INSERT [dbo].[ODS] ([Id], [OdsBranch], [OdsPassword]) VALUES (7, N'string', N'string')
INSERT [dbo].[ODS] ([Id], [OdsBranch], [OdsPassword]) VALUES (8, N'string', N'string')
SET IDENTITY_INSERT [dbo].[ODS] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([Id], [Name]) VALUES (1, N'Staff')
INSERT [dbo].[Role] ([Id], [Name]) VALUES (2, N'Admin')
INSERT [dbo].[Role] ([Id], [Name]) VALUES (3, N'Manager')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Source] ON 

INSERT [dbo].[Source] ([Id], [SourceName]) VALUES (1, N'high')
INSERT [dbo].[Source] ([Id], [SourceName]) VALUES (2, NULL)
INSERT [dbo].[Source] ([Id], [SourceName]) VALUES (4, N'cuong')
SET IDENTITY_INSERT [dbo].[Source] OFF
GO
SET IDENTITY_INSERT [dbo].[TicketLevel] ON 

INSERT [dbo].[TicketLevel] ([Id], [LevelName]) VALUES (1, N'high')
INSERT [dbo].[TicketLevel] ([Id], [LevelName]) VALUES (2, N'low')
SET IDENTITY_INSERT [dbo].[TicketLevel] OFF
GO
SET IDENTITY_INSERT [dbo].[TicketStatus] ON 

INSERT [dbo].[TicketStatus] ([Id], [StatusName]) VALUES (1, N'Sửa chửax')
INSERT [dbo].[TicketStatus] ([Id], [StatusName]) VALUES (2, N'Xử lý')
INSERT [dbo].[TicketStatus] ([Id], [StatusName]) VALUES (3, N'Hỗ Trợ')
SET IDENTITY_INSERT [dbo].[TicketStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[TicketTag] ON 

INSERT [dbo].[TicketTag] ([Id], [TagName]) VALUES (1, N'Sự Cố')
INSERT [dbo].[TicketTag] ([Id], [TagName]) VALUES (2, N'Xử lý')
INSERT [dbo].[TicketTag] ([Id], [TagName]) VALUES (3, N'Sữa Chữa')
SET IDENTITY_INSERT [dbo].[TicketTag] OFF
GO
SET IDENTITY_INSERT [dbo].[TicketType] ON 

INSERT [dbo].[TicketType] ([Id], [TicketTypeName], [KpiDuration]) VALUES (1, N'x', N'x')
SET IDENTITY_INSERT [dbo].[TicketType] OFF
GO
ALTER TABLE [dbo].[Branch]  WITH CHECK ADD  CONSTRAINT [FK_Branch_ODS] FOREIGN KEY([OdsId])
REFERENCES [dbo].[ODS] ([Id])
GO
ALTER TABLE [dbo].[Branch] CHECK CONSTRAINT [FK_Branch_ODS]
GO
ALTER TABLE [dbo].[CallHistory]  WITH CHECK ADD  CONSTRAINT [FK_CallHistory_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[CallHistory] CHECK CONSTRAINT [FK_CallHistory_Customer]
GO
ALTER TABLE [dbo].[CallHistory]  WITH CHECK ADD  CONSTRAINT [FK_CallHistory_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[CallHistory] CHECK CONSTRAINT [FK_CallHistory_User]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_Branch] FOREIGN KEY([BranchId ])
REFERENCES [dbo].[Branch] ([Id])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_Branch]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_Customer]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Branch] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branch] ([Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Branch]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_CustomerLevel] FOREIGN KEY([CustomerLevelId])
REFERENCES [dbo].[CustomerLevel] ([Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_CustomerLevel]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Source] FOREIGN KEY([SourceId])
REFERENCES [dbo].[Source] ([Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Source]
GO
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Ticket] FOREIGN KEY([TicketId])
REFERENCES [dbo].[Ticket] ([Id])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Ticket]
GO
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_Customer]
GO
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_User] FOREIGN KEY([StaffId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_User]
GO
ALTER TABLE [dbo].[Survey]  WITH CHECK ADD  CONSTRAINT [FK_Survey_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Survey] CHECK CONSTRAINT [FK_Survey_Customer]
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_Branch] FOREIGN KEY([Branch_Id])
REFERENCES [dbo].[Branch] ([Id])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_Branch]
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_Customer]
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_TicketLevel] FOREIGN KEY([LevelId])
REFERENCES [dbo].[TicketLevel] ([Id])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_TicketLevel]
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_TicketStatus] FOREIGN KEY([TicketStatusId])
REFERENCES [dbo].[TicketStatus] ([Id])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_TicketStatus]
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_TicketType] FOREIGN KEY([TicketTypeId])
REFERENCES [dbo].[TicketType] ([Id])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_TicketType]
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_User] FOREIGN KEY([AssignedUserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_User]
GO
ALTER TABLE [dbo].[TicketTagConnect]  WITH CHECK ADD  CONSTRAINT [FK_TicketTagConnect_Ticket] FOREIGN KEY([TicketId])
REFERENCES [dbo].[Ticket] ([Id])
GO
ALTER TABLE [dbo].[TicketTagConnect] CHECK CONSTRAINT [FK_TicketTagConnect_Ticket]
GO
ALTER TABLE [dbo].[TicketTagConnect]  WITH CHECK ADD  CONSTRAINT [FK_TicketTagConnect_TicketTag] FOREIGN KEY([TicketTagId])
REFERENCES [dbo].[TicketTag] ([Id])
GO
ALTER TABLE [dbo].[TicketTagConnect] CHECK CONSTRAINT [FK_TicketTagConnect_TicketTag]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Branch] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branch] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Branch]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO
/****** Object:  StoredProcedure [dbo].[Branch_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Branch_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
    @Ods_Id int = NULL,
	@Address nvarchar(255) = NULL,
	@BranchName nvarchar(255) = NULL
	
AS
BEGIN
    SET NOCOUNT ON;
    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM Branch;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[CallHistory_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CallHistory_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
    @CallNumber nvarchar(255) = NULL,
    @UserId int = NULL,
    @CustomerId int = NULL,
	@RealTimeCall nvarchar(255) = NULL,
    @StatusCall nvarchar(255) = NULL,
    @DateCall datetime = NULL,
    @RecordLink nvarchar(255) = NULL,
	@Direction nvarchar(255) = NULL,
    @TotalTimeCall datetime = NULL  
AS
BEGIN
    SET NOCOUNT ON;
    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM CallHistory;
    END
	 
	ELSE IF @Operation = 'SEARCH_DAY'
    BEGIN 
	 SELECT *
    FROM CallHistory
    WHERE DateCall BETWEEN @DateCall AND @DateCall;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[Contract_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Contract_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
    @CustomerId int = NULL,
	@ContractType nvarchar(255) = NULL,
	@TermsAndConditions nvarchar(255) = NULL,
	@TimeStart datetime = NULL,
	@TimeEnd datetime = NULL,
	@LastEditedTime datetime = NULL
AS
BEGIN
    SET NOCOUNT ON;
    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM Contract;
    END
	
	ELSE IF @Operation = 'SEARCH_ID'
    BEGIN     
        SELECT * FROM Contract Where Id = @Id;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[Customer_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Customer_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
    @CustomerLevelId int = NULL,
	@SourveId int = NULL,
	@BranchId int = NULL,
	@LastName nvarchar(255) = NULL,
	@FirstName nvarchar(255) = NULL,
	@PhoneNumber nvarchar(255) = NULL,
	@Status nvarchar(255) = NULL,
	@Gender nvarchar(255) = NULL,
	@Address nvarchar(255) = NULL,
	@DayOfBirth datetime = NULL,
	@DateCreated datetime = NULL,
	@LastEditedTime datetime = NULL,
	@Name nvarchar(255) = NULL
AS
BEGIN
    SET NOCOUNT ON;
    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM Customer;
    END

	ELSE IF @Operation = 'NAME'
    BEGIN 
	SELECT * FROM Customer WHERE [Name] LIKE '%' + @Name + '%'
    END

	ELSE IF @Operation = 'SEARCH_ID'
    BEGIN     
        SELECT * FROM Customer Where Id = @Id;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[Level_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Level_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
    @Level_Name nvarchar(255) = NULL
	
AS
BEGIN
    SET NOCOUNT ON;
    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM Level;
    END

	ELSE IF @Operation = 'NAME'
    BEGIN 
	SELECT *
    FROM Level
    WHERE Level_Name LIKE '%' + @Level_Name + '%';
    END

	ELSE IF @Operation = 'SEARCH_ID'
    BEGIN     
        SELECT * FROM Level Where Id = @Id;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[Log_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROCEDURE [dbo].[Log_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
    @Note nvarchar(255) = NULL,
	@CreatedDate datetime = NULL,
	@IsAssigned nvarchar(255) = NULL,
	@AssignedTo nvarchar(255) = NULL,
	@Reason nvarchar(255) = NULL,
	@TimeStart datetime = NULL,
	@TimeEnd datetime = NULL,
	@UserId int = NULL
	
AS
BEGIN
    SET NOCOUNT ON;
    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM Log;
    END

	ELSE IF @Operation = 'DATE'
    BEGIN 
	SELECT *
    FROM Log
    WHERE TimeEnd BETWEEN @TimeStart AND @TimeEnd;;
    END

	ELSE IF @Operation = 'USERID'
    BEGIN     
        SELECT * FROM Log Where UserId = @UserId;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[ODS_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ODS_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
    @OdsBranch nvarchar(255) = NULL,
	@OdsPassword nvarchar(255) = NULL
	
AS
BEGIN
    SET NOCOUNT ON;
    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM ODS;
    END

	
END
GO
/****** Object:  StoredProcedure [dbo].[Survey_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Survey_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
	@SatisfactionRating nvarchar(255) = NULL,
	@Comment nvarchar(255) = NULL,
	@SurveyDate datetime = NULL,
	@CustomerId int = NULL

AS
BEGIN
    SET NOCOUNT ON;
    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM Survey;
    END

	ELSE IF @Operation = 'CUSTOMERID'
    BEGIN     
        SELECT * FROM Survey Where CustomerId = @CustomerId;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[Ticket_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Ticket_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
	@Call_Id int = NULL,
	@Note nvarchar(255) = NULL,
	@CustomerId int = NULL,
	@TicketTypeId int = NULL,
	@LevelId int = NULL,
	@LevelStatusId int = NULL,
	@CreateBy datetime = NULL

AS

BEGIN
    SET NOCOUNT ON;

    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM Ticket;
    END

	ELSE IF @Operation = 'ID'
    BEGIN     
        SELECT * FROM Ticket Where Id = @Id;
    END

	ELSE IF @Operation = 'DATE'
    BEGIN     
    SELECT *
    FROM Ticket
    WHERE CreatedBy BETWEEN @CreateBy AND @CreateBy;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[TicketImage_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[TicketImage_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
	@LogId int = NULL,
	@ImageUrl nvarchar(255) = NULL
	

AS
BEGIN
    SET NOCOUNT ON;
    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM TicketImage;
    END

END
GO
/****** Object:  StoredProcedure [dbo].[User_SP]    Script Date: 5/6/2024 2:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[User_SP]
    @Operation nvarchar(10),
	@Id int = NULL,
	@RoleId int = NULL,
	@LastName nvarchar(255) = NULL,
	@UserName nvarchar(255) = NULL,
	@Password nvarchar(255) = NULL,
	@Status nvarchar(255) = NULL,
	@Gender nvarchar(255) = NULL,
	@Address nvarchar(255) = NULL,
	@DayOfBirth datetime = NULL,
	@CreatedDate datetime = NULL
	
AS
BEGIN
    SET NOCOUNT ON;
    IF @Operation = 'READ'
    BEGIN     
        SELECT * FROM [User];
    END

END
GO
USE [master]
GO
ALTER DATABASE [newtelecallbe] SET  READ_WRITE 
GO
